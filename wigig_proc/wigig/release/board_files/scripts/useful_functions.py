#-------------------------------------------------------------------------------
# Name:        useful_functions
# Purpose:     Supply helper functions for other modules
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

import xml.etree.ElementTree as ET
from customizations import *

def init_CONSTANTS():
    # Define dictionary to be used everywhere

    str1='MAX_BRP_PER_TILE'
    str2='MAX_BRP_OVERALL_SHORT_RANGE'
    str3='MAX_BRP_OVERALL'

    str4 = 'CABLE_LOSS'
    str5 = 'TPC'
    str6 = 'LO_TARGET_POWER'
    str7 = 'DC_SOURCE'

    #define "global" constants
    CONSTANTS={'MAX_RFC': 7,
               'DEFAULT_'+str4: 8.0,
               'DEFAULT_'+str5: 10.0,
               'DEFAULT_'+str6: 0.0,
               'DEFAULT_POUT' : 10.0,
               'DEFAULT_UNUSED_BRP_ANTENNAS': [7,8],
               'DEFAULT_'+str1 : 10,
               'DEFAULT_'+str2 : 6,
               'DEFAULT_'+str3 : 256,
               'MAX_SECTORS_MASSIVE' : 126,
               'MAX_SECTORS_SENS_TX' : 8,
               'MAX_SECTORS_SENS_RX' : 0,
               'SECTOR_TABLE_SIZE' : 128,
               'FW_CONST_NUM_DIVERSITY_SECTORS': 64,
               'EDGES_PER_CHIP' : 32,
               'DISTS_PER_CHIP' : 8,
               'ILLEGAL_SECTORS': [38, 68]}
    
    CONSTANTS['TAG_NAMES']={'CHANNEL': 'Channel', #appears in both xmls
                             'COMMON_TYPE_TABLES' : 'Common',
                             'CABLE_LOSS' : 'Cable_Loss',
                             'LO_TARGET_POWER' : 'LO_Target_Power',
                             'RF_SETS' : 'RFsets',
                             'RF_SENS_SETS' : 'RFsens_sets',
                             'TSSI_CALIB': 'tx_Power_Calibration',
                             'TPC' : 'TPC',
                             'UNUSED_BRP' : 'Unused_BRP_Antennas',
                             'BRP_ANTENNAS' : 'antennas',
                             'RF_SET': 'RFset',
                             'RFC' : 'RFC',
                             'RX' : 'rx',
                             'TX' : 'tx',
                             'SECTOR' : 'sector',
                             'TSSI_TX_SECTOR' : 'tx_sector',
                             'TSSI_RX_SECTOR' : 'rx_sector',
                             'TSSI_TX_CHAIN' : 'chain_number',
                             'ANTENNAS' : 'antennas',
                             'PHASES' : 'Phases',
                             'ETYPES' : 'ETypes',
                             'DTYPES' : 'DTypes',
                             'x8' : 'x8',
                             'x16' : 'x16',
                             'x1_gc_offset' : 'x1_gc_offset',
                             'GROUP' : 'Group',
                             'SENS_SAMPLE_OFFSET' : 'sensing_sample_offset',
                             'ID_NUMBER' : 'ID_number',
                             'ID_IDENT' : 'identifier',
                             'SILICON_NAME' : 'silicon',
                             'MAKER' : 'MAKER',
                             'GENERAL_PARAMS' : 'general_parameters',
                             'GENERAL_PARAMS_MODE' : 'general_parameters_mode',
                             'WATERMARK' : 'watermark',
                             'T_BURST' : 't_burst',
                             'N_BURSTS' : 'n_bursts',
                             'T_PULSE' : 't_pulse',
                             'N_PULSES' : 'n_pulses',
                             'N_SAMPLES' : 'n_samples',
                             'FIRST_SAMPLE_OFFSET' : 'first_sample_offset',
                             'SAMPLES_TO_AVG' : 'samples_to_avg',
                             'SAMPLES_TO_AVG' : 'samples_to_avg',
                             'PULSES_TO_AVG' : 'pulses_to_avg',
                             'ENABLE_VECTORS' : 'enable_vectors',
                             'ENABLE_VECTOR_MODE' : 'enable_vector_mode',
                             'ACTIVE_VEC_CONFIG' : 'active_vector_config',
                             'PULSE_ACTIVE_VEC' : 'pulse_active_vector',
                             'PULSE_PARAMS' : 'pulse_parameters',
                             'PULSE_PARAMS_MODE' : 'pulse_parameters_mode',
                             'TXRX_COMB' : 'txrx_combinations',
                             'TX_SEC' : 'tx_sector',
                             'RX_SEC' : 'rx_sector'}
    CONSTANTS['TAG_NAMES']['BRP_FIELDS']={
        str1 : 'max_BRP_Per_Tile',
        str2 : 'max_BRP_Overall_Short_Range',
        str3 : 'max_BRP_Overall',
        }
    CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC'] = {  # these must come from the xml file
        str4: {'TITLE': 'Cable_Loss', 'FIELD': 'attenuation'},
        str5: {'TITLE': 'TPC', 'FIELD': 'Pout'},
        str6: {'TITLE': 'LO_Target_Power', 'FIELD': 'Target_Power'},
        str7: {'TITLE': 'DC_Source', 'FIELD': 'Voltage_Source'}
        }
    CONSTANTS['ATTRIB_NAMES']={'NUM' : 'Num',
                               'NAME' : 'Name'}
    CONSTANTS['CB1_CHANNELS']=(1,2,3,4,5,6)
    CONSTANTS['CB2_CHANNELS']=(9,10,11,12)
    CONSTANTS['SUPPORTED_CHANNELS']=CONSTANTS['CB1_CHANNELS']+ \
        CONSTANTS['CB2_CHANNELS']
    CONSTANTS['DEFAULT_TYPES']={'TX_DTYPE' : 2,
                            'TX_ETYPE' : 2,
                            'RX_DTYPE' : 2,
                            'RX_ETYPE' : 2,
                            'MAX_INACTIVE_DTYPE' : 1,
                            'MAX_INACTIVE_ETYPE' : 1,
                            'PWDN_TYPE' : 0}

    CONSTANTS['EDGES_PER_DIST']=CONSTANTS['EDGES_PER_CHIP']/CONSTANTS['DISTS_PER_CHIP']
    CONSTANTS['DISTS_PER_SPDT']=2
    CONSTANTS['ALL_TABLES_LIST']=['tx_gain_offset_etype',
                     'tx_iref_etype',
                     'tx_iref_offset_edge',
                     'rx_gain_offset_etype',
                     'rx_iref_etype',
                     'rx_iref_offset_edge',
                     'edge_gain_conversion_tbl',
                     'tx_gain_offset_dtype',
                     'tx_iref_dtype',
                     'tx_iref_offset_dist',
                     'rx_gain_offset_dtype',
                     'rx_iref_dtype',
                     'rx_iref_offset_dist',
                     'dist_gain_conversion_tbl',
                     'tx_iref_rtype',
                     'rx_iref_rtype',
                     'tx_gain_itype',
                     'tx_iref_itype',
                     'rx_gain_itype',
                     'rx_iref_itype',
                     'rx_fine_gain_itype',
                     'tx_gain_ltype',
                     'rx_gain_ltype',
                     'pwdn_ltype',
                     'iref_ltype',
                     'additional_fixes'
                     ]
    CONSTANTS['INTERNAL']={'TSSI_STRUCT' : 'TSSICalibStructure',
        'BRP_STRUCT' : 'BRPStructure',
        'SCALAR_PER_RFC_STRUCT' : 'ScalarPerRFCStructure'}
	# DC Source defaults
    CONSTANTS[str7] = {'EXTERNAL': 0.0, 'XIF': 1.0}
    CONSTANTS['DEFAULT_' + str7] = CONSTANTS[str7]['XIF']

    CONSTANTS['SENS_MODE']= ['Search','Face','Gesture','Test']

    return CONSTANTS

def load_xml(filepath):
    xml_tree=ET.parse(filepath)
    xml_root=xml_tree.getroot()
    return xml_tree

def NumSubElementsSpecificName(father_element,tag_name):
     #
     # This function finds the number of sub-elements
     # that have a specific name
     #

     relevant_sub_elements=father_element.findall(tag_name)

     return len(relevant_sub_elements)


def specific_labels_in_one_tag(father_element,tag_name,attrib_name):
    #
    # This function extracts the 'attrib_name' attribute
    # of all tags named <tag_name> inside <father_element>
    #

    #find all <tag_name> elements inside father element (no recursion)
    all_tag_names_list=father_element.findall(tag_name);

    #initialize output
    output_list=[];

    for cnt_occurrence in range(0,len(all_tag_names_list)):
        try:
            output_list.append(int(all_tag_names_list[cnt_occurrence].attrib[attrib_name]))
        except ValueError:
            try:
                output_list.append(list(map(int,(all_tag_names_list[cnt_occurrence].attrib[attrib_name].split()))))
            except ValueError:
                output_list.append(all_tag_names_list[cnt_occurrence].attrib[attrib_name])
        except Exception as e:
            raise

    return output_list


def parse_list_numeric_strings(string_in):
    #
    # Converts string of numbers to numerical list
    #

    if string_in is None:
        return []
    else:
        return list(map(int,string_in.split()))

def list_of_identical_elements(in_list):
    #
    # This function returns a boolean answer to the question: Are all elements
    # in list identical?
    #

    return (all(x==in_list[0] for x in in_list))

def list_to_bit_vector(bit_list):
    bit_vector = 0
    for bit in bit_list:
        bit_vector |= (1 << bit)

    return bit_vector

def MATLAB_find_boolean_input(input_list):
    #
    # This function is equivalent to the MATLAB 'find' function with a boolean
    # vector input
    #

    index_list=[]
    for cnt in range(0,len(input_list)):
        if input_list[cnt]:
            index_list.append(cnt)

    return index_list

def translate_struct_to_xml_tag(str):
    #
    #
    #

    CONSTANTS=init_CONSTANTS()

    if str==CONSTANTS['INTERNAL']['TSSI_STRUCT']:
        return CONSTANTS['TAG_NAMES']['TSSI_CALIB']

    if str==CONSTANTS['INTERNAL']['BRP_STRUCT']:
        return CONSTANTS['TAG_NAMES']['UNUSED_BRP']

    if str==CONSTANTS['INTERNAL']['CABLE_STRUCT']:
        return CONSTANTS['TAG_NAMES']['CABLE_LOSS']