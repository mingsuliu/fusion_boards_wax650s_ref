#-------------------------------------------------------------------------------
# Name:        module main_function
# Purpose:     This function validates a collection of two xml files,
#              and generates a Python data structure from one of them
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

from validate_type_tables import validate_classification_xml
from GP_labels import validate_and_complete_GP_labels
from GP_labels import validate_and_complete_GP_sens_labels
from useful_functions import *
from sectors import *
from validate_data_structure import post_process
from gen_ini_output import generate_ini_output
from validate_sens_params import *
import optparse
import os
import sys


def validate_and_complete_xmls(sectors_xml_path,
        classification_xml_path,sensing_sectors_xml_path, sensing_params_xml_path):

    CONSTANTS=init_CONSTANTS();
    error_msg=''

    ######### General Parameters Labels ###########
    try:
        sectors_root=load_xml(sectors_xml_path)
    except IOError as e:
        print("sector xml file not found")
        raise
    except Exception as e:
        print("Unknown error loading sector xml file")
        raise

    #do actual work 
    try:
        GP_labels,RF_sets_structure,board_file_type,fw_version = validate_and_complete_GP_labels(sectors_root)
    except Exception as e:
        raise

    ############# Lineups & Type Tables################
    try:
        classification_root=load_xml(classification_xml_path)
    except IOError as e:
        print("classification xml file not found")
        raise
    except Exception as e:
        print("Unknown error loading classification xml file")
        raise

    try:
        rf_config_channel_groups=validate_classification_xml(
            classification_root)
    except Exception as e:
        error_msg+=e.message

    ################# Sectors ####################
    try:
        SectorsStruct,sectors_channel_dict=generate_sector_struct(
            sectors_root.find(CONSTANTS['TAG_NAMES']['RF_SETS']),
            RF_sets_structure)
    except Exception as e:
        error_msg+=e.message

    assert(error_msg==''),error_msg

    ############# Sensing Labels ################
    if sensing_sectors_xml_path.endswith(".xml"):
        try:
            sensing_root=load_xml(sensing_sectors_xml_path)
        except IOError as e:
            print("Sensing xml file not found")
            raise
        except Exception as e:
            print("Unknown error loading sensing xml file")
            raise
        try:
            GP_sens_labels,RF_sens_sets_structure,sens_params_type= \
                validate_and_complete_GP_sens_labels(sensing_root)
        except Exception as e:
            error_msg+=e.message
    else:
        sensing_root = {}
        sens_params_type = 'sensing-params'
        RF_sens_sets_structure = {}
        GP_sens_labels = {}

    ################# Sensing Sectors ####################
    try:
        if sensing_root:
            SectorsStruct_sens,sectors_channel_sens_dict=generate_sector_struct(sensing_root.find(CONSTANTS['TAG_NAMES']['RF_SENS_SETS']), RF_sens_sets_structure)
        else:
            sectors_channel_sens_dict = {}
            SectorsStruct_sens = {}

    except Exception as e:
        error_msg+=e.message

    assert(error_msg==''),error_msg

    ################# Sensing Params ####################
    general_params_structure = {}
    enable_vectors_structure = {}
    pulse_params_structure = {}
    if sensing_sectors_xml_path.endswith(".xml"):
        if sensing_params_xml_path.endswith(".xml"):
            try:
                sens_params_root=load_xml(sensing_params_xml_path)
            except IOError as e:
                print("Sensing params xml file not found")
                raise
            except Exception as e:
                print("Unknown error loading sensing params xml file")
                raise

            try:
                general_params_structure, enable_vectors_structure, pulse_params_structure, ID_dict = validate_sens_params_xml(sens_params_root)
                GP_sens_labels['ID_sens_params'] = ID_dict
            except Exception as e:
                error_msg+=e.message
        else:
            raise Exception('Sensing_params.xml is missing!')

    assert(error_msg==''),error_msg

    #pack output
    dark_blue_output={'BoardFileType': board_file_type,
         'GP_Labels' : GP_labels,
         'Classification' : classification_root,
         'RFConfigChannelGroups' : rf_config_channel_groups,
         'Sectors' : SectorsStruct,
         'SectorChannelGroups' : sectors_channel_dict,
         'Sectors_sens' : SectorsStruct_sens,
         'SectorChannelGroups_sens' : sectors_channel_sens_dict,
         'GP_Sens_Labels' : GP_sens_labels,
         'Sens_General_Parameters' : general_params_structure,
         'Sens_Enable_Vectors' : enable_vectors_structure,
         'Sens_Pulse_Params' : pulse_params_structure,
         'FwVersion' : fw_version}

    try:
        dark_blue_output=post_process(dark_blue_output)
    except Exception as e:
        raise

    return dark_blue_output


def read_version_information(version_file_path):
    
    version = major_string = minor_string = sub_minor_string = fw_rev_build_string = ""
    
    lines = list()
    
    if "" == version_file_path:
        return version
    
    if not os.path.exists(version_file_path):
        return version
  
    #open file
    if type(version_file_path) in [str, unicode]:
        try:
            h_file = open(version_file_path, 'r')
        except:
            return version
    else:
        return version
    
    for line in h_file:
    
        lines.append(line.strip())
               
    h_file.close()
    
    for line in lines:
        
        splits = line.split()
        
        for i in range(0, len(splits)):
            if splits[i] == "MAJOR" and i < len(splits):
                major_string = splits[i+1]
                
            if splits[i] == "MINOR" and i < len(splits):
                minor_string = splits[i+1]
                
            if splits[i] == "SUB_MINOR" and i < len(splits):
                sub_minor_string = splits[i+1]
                
            if splits[i] == "FW_REV_BUILD" and i < len(splits):
                fw_rev_build_string = splits[i+1]
    
    if major_string != "" and minor_string != "" and sub_minor_string != "" and fw_rev_build_string != "":
        version = major_string + minor_string + sub_minor_string + fw_rev_build_string

    return version


def xmls_to_board_file(sectors_xml_path,classification_xml_path,sensing_sectors_xml_path, sensing_params_xml_path, version_file_path, output_file_name):
    #
    # This is the "executable" function
    #

    #call parser
    try:
        dark_blue_output=validate_and_complete_xmls(sectors_xml_path,
            classification_xml_path,sensing_sectors_xml_path, sensing_params_xml_path)
        print("Parser succeeded in converting xml file to Python data structure")
        
        #check if need to override FW version with user input
        fw_version = read_version_information(version_file_path)
        if fw_version != "":
            dark_blue_output['FwVersion'] = fw_version
        
        generate_ini_output(dark_blue_output, output_file_name)
        print(output_file_name + " created successfully")
    except Exception as e:
        print(e.message)
        print("Terminating program due to error")
        return False
    
    return True



if __name__ == '__main__':
    
    parser = optparse.OptionParser()

    parser.add_option('-s', '--sectors', action="store", dest="sectors", help="sectors XML file name", default="")
    parser.add_option('-c', '--classification', action="store", dest="classification", help="classification XML file name", default="")
    parser.add_option('-r', '--sense sectors', action="store", dest="sense_sectors", help="sensing sectors XML file name", default="")
    parser.add_option('-t', '--sense params', action="store", dest="sense_params", help="sensing params XML file name", default="")
    parser.add_option('-o', '--output', action="store", dest="output", help="output file name", default="default.ini")
    parser.add_option('-v', '--version', action="store", dest="version", help="version file name", default="")

    options, args = parser.parse_args()

    print 'sectors file:', options.sectors
    print 'classification file:', options.classification
    print 'sensing sectors file:', options.sense_sectors
    print 'sensing params file:', options.sense_params
    print 'version file:', options.version
    print 'output file:', options.output

    res = xmls_to_board_file(options.sectors, options.classification, options.sense_sectors, options.sense_params, options.version, options.output)
    
    if True == res:
        sys.exit(0)
    else:
        sys.exit(-1)