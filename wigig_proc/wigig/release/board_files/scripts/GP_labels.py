#-------------------------------------------------------------------------------
# Name:        GP_Labels module
# Purpose:
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

import xml.etree.ElementTree as ET
import copy
from useful_functions import *
from sector_class import Sector
from sectors import generate_sector_channel_dict
from customizations import *

def validate_and_complete_GP_labels(sectors_root):
    # This function validates the input sectors
    # *.xml file and also fills in missing default
    # data

    CONSTANTS=init_CONSTANTS()

    #initialize
    ScalarPerRFCStructure={}

    #convert xml structure to Python nested dictionary structure
    RF_sets_structure=extract_RF_sets_structure(
        sectors_root.find(CONSTANTS['TAG_NAMES']['RF_SETS']))

    #validate RFC labels from inside <RFsets> tag
    if not validate_RFC_tags(RF_sets_structure):
        raise ValueError("Illegal RF sets structure")

    #validate sector structure
    try:
        board_file_type=validate_sector_structure(RF_sets_structure)
    except AssertionError as e:
        raise

    #generate RFset dictionary
    try:
        RF_set_dict=generate_RFsets_dict(RF_sets_structure)
    except ValueError as e:
        raise e

    # apply mapping to <DC_Source> element
    sectors_root = map_dc_source_element(sectors_root)

    # process all "scalar per rfc" labels
    for field_name in CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC']:
        ScalarPerRFCStructure[field_name]=generate_scalar_per_rfc(sectors_root,field_name)


    #process TSSI calib label
    try:
        TSSICalibStructure=get_TSSI_calib(sectors_root.find(
            CONSTANTS['TAG_NAMES']['TSSI_CALIB']),RF_set_dict)
    except:
        raise


    #process "oscillating antennas" label
    try:
        BRPStructure=get_unused_BRP_ants(
            sectors_root.find(CONSTANTS['TAG_NAMES']['UNUSED_BRP']))
    except AssertionError:
##        print("Each RFC must have at least two unused BRP antennas")
        raise


    #MAX BRP fields
    for field_name in CONSTANTS['TAG_NAMES']['BRP_FIELDS']:
        BRPStructure[field_name]=generate_BRP_field_parameter(sectors_root,field_name)

    assert(BRPStructure['MAX_BRP_OVERALL']>=BRPStructure['MAX_BRP_PER_TILE']),\
            "Error: MAX_BRP_OVERALL<MAX_BRP_PER_TILE"
    assert(BRPStructure['MAX_BRP_OVERALL']>=BRPStructure['MAX_BRP_OVERALL_SHORT_RANGE']),\
            "Error: MAX_BRP_OVERALL<MAX_BRP_OVERALL_SHORT_RANGE"

    #Silicon ID
    silicon = sectors_root.find(CONSTANTS['TAG_NAMES']['SILICON_NAME']).text

    #read ID number
    ID_dict=dict.fromkeys(['identifier','maker'])
    ID_dict['identifier']=sectors_root.find(CONSTANTS['TAG_NAMES']['ID_NUMBER']).find('identifier').text
    ID_dict['maker']=int(sectors_root.find(CONSTANTS['TAG_NAMES']['ID_NUMBER']).find('maker').text)

    #lineups group
    lineups=sectors_root.find('lineups_group').text

    #Check if FW version resides in XML
    fw_version_entry = sectors_root.find('FW_version')
    if fw_version_entry != None:
        fw_version = fw_version_entry.text
        fw_version = fw_version.replace(".", "")
    else:
        fw_version = ""

    #pack output
    GP_labels={'RFsets_dict' : RF_set_dict,
                CONSTANTS['INTERNAL']['SCALAR_PER_RFC_STRUCT'] : ScalarPerRFCStructure,
                CONSTANTS['INTERNAL']['TSSI_STRUCT'] : TSSICalibStructure,
                CONSTANTS['INTERNAL']['BRP_STRUCT'] : BRPStructure,
                'ID': ID_dict,
                'Lineups': lineups,
                CONSTANTS['TAG_NAMES']['SILICON_NAME']: silicon}
    return GP_labels,RF_sets_structure,board_file_type,fw_version

def validate_and_complete_GP_sens_labels(sensing_root):
    # This function validates the input sectors
    # *.xml file and also fills in missing default
    # data

    CONSTANTS=init_CONSTANTS()

    #initialize
    ScalarPerRFCStructure={}

    #convert xml structure to Python nested dictionary structure
    RF_sens_sets_structure=extract_RF_sets_structure(
        sensing_root.find(CONSTANTS['TAG_NAMES']['RF_SENS_SETS']))


    #validate RFC labels from inside <RFsets> tag
    #if not validate_RFC_tags(RF_sens_sets_structure):
    #    raise ValueError("Illegal RF sets structure")

    #validate sector structure
    try:
        sens_params_type=validate_sens_sector_structure(RF_sens_sets_structure)
    except AssertionError as e:
        raise

    #generate RFset dictionary
    try:
        RF_set_dict=generate_RF_sens_sets_dict(RF_sens_sets_structure)

    except ValueError as e:
        raise e

    #process "oscillating antennas" label
    try:
        BRPStructure=get_unused_BRP_ants(
            sensing_root.find(CONSTANTS['TAG_NAMES']['UNUSED_BRP']))
    except AssertionError:
##        print("Each RFC must have at least two unused BRP antennas")
        raise

    #Silicon ID
    silicon = sensing_root.find(CONSTANTS['TAG_NAMES']['SILICON_NAME']).text

    #read ID number
    ID_dict=dict.fromkeys(['identifier','maker'])
    ID_dict['identifier']=sensing_root.find(CONSTANTS['TAG_NAMES']['ID_NUMBER']).find('identifier').text
    ID_dict['maker']=int(sensing_root.find(CONSTANTS['TAG_NAMES']['ID_NUMBER']).find('maker').text)

    #read sensing sample offset
    sensing_sample_offset = int(sensing_root.find(CONSTANTS['TAG_NAMES']['SENS_SAMPLE_OFFSET']).text)

    #pack output
    GP_sens_labels={'RFsets_dict' : RF_set_dict,
                CONSTANTS['INTERNAL']['BRP_STRUCT'] : BRPStructure,
                'ID_sens_sectors': ID_dict,
                'ID_sens_params': {},
                CONSTANTS['TAG_NAMES']['SILICON_NAME']: silicon,
                CONSTANTS['TAG_NAMES']['SENS_SAMPLE_OFFSET']: sensing_sample_offset}

    return GP_sens_labels,RF_sens_sets_structure,sens_params_type

def extract_RF_sets_structure(RF_sets_element):
    # This function creates a nested dictionary
    # (equivalent to nested struct). The hierarchy is
    # RF set -> channel -> tx,rx -> RFC
    # and the value of the innermost key is
    # a list of sector numbers

    CONSTANTS=init_CONSTANTS()

    rf_sets_list=RF_sets_element.findall(CONSTANTS['TAG_NAMES']['RF_SET'])

    #define constant
    Nrf_sets=len(rf_sets_list)

    #initialize output
    RFsets_dict=dict.fromkeys(range(0,Nrf_sets));

    #loop over rf sets
    for cnt_rf_set in range(0,Nrf_sets):

        #which channels appear in current rf set
        RFsets_dict[cnt_rf_set]=dict.fromkeys(specific_labels_in_one_tag(
            rf_sets_list[cnt_rf_set],CONSTANTS['TAG_NAMES']['CHANNEL'],
            CONSTANTS['ATTRIB_NAMES']['NUM']))
        channel_element_list=rf_sets_list[cnt_rf_set].findall(
            CONSTANTS['TAG_NAMES']['CHANNEL'])

        #define constant
        Nch_in_curr_rf_set=len(RFsets_dict[cnt_rf_set])

        #loop over channels
        for cnt_channel in range(0,Nch_in_curr_rf_set):
            #get channel number
            curr_channel=RFsets_dict[cnt_rf_set].keys()[cnt_channel]

            #init dictionary
            RFsets_dict[cnt_rf_set][curr_channel]=dict.fromkeys(
                [CONSTANTS['TAG_NAMES']['RX'],CONSTANTS['TAG_NAMES']['TX']])

            #loop over rx,tx
            for curr_mode in RFsets_dict[cnt_rf_set][curr_channel].keys():
                #get element
                rxtx_element=channel_element_list[cnt_channel].find(curr_mode)

                #init dictionary
                RFsets_dict[cnt_rf_set][curr_channel][curr_mode]=\
                    dict.fromkeys(specific_labels_in_one_tag(
                    rxtx_element,CONSTANTS['TAG_NAMES']['RFC'],
                    CONSTANTS['ATTRIB_NAMES']['NUM']))
                NRFCs=len(RFsets_dict[cnt_rf_set][curr_channel][curr_mode])

                #loop over RFCs
                for cnt_RFC in range(0,NRFCs):
                    #get RFC number
                    curr_RFC=RFsets_dict[cnt_rf_set][curr_channel][curr_mode]\
                        .keys()[cnt_RFC]

                    #get list of sectors from current RFC
                    RFsets_dict[cnt_rf_set][curr_channel][curr_mode][curr_RFC]=\
                        specific_labels_in_one_tag(
                        rxtx_element.findall(CONSTANTS['TAG_NAMES']['RFC'])\
                        [cnt_RFC],CONSTANTS['TAG_NAMES']['SECTOR'],
                        CONSTANTS['ATTRIB_NAMES']['NUM'])

    return RFsets_dict

def validate_RFC_tags(RF_sets_structure):
    # This function perfoms the following validations
    # 1) All RF sets have the same channels defined
    # 2) Inside each RF set, all tx and rx tags have the same "RF vectors"
    # 3) If RFC = -1 is used, it is a single-tile board file
    #

    CONSTANTS=init_CONSTANTS()

    RFsets=RF_sets_structure.keys()

    #get channel list from first RFset
    correct_channel_list=RF_sets_structure[RFsets[0]].keys()

    #initialize output
    return_value=True;

    #init
    ch_vec_list=[]

    #loop over all rf sets
    for curr_rf_set in range(0,len(RFsets)):

        #all channels in current RFset
        ch_vec_list.append(RF_sets_structure[RFsets[curr_rf_set]].keys())

        #initialize list of rfc vectors
        rfc_vec_list_rx=[]
        rfc_vec_list_tx=[]

        #loop over all channels for 2)
        for curr_channel in RF_sets_structure[curr_rf_set].keys():
            #easier notation
            rx_dict=RF_sets_structure[curr_rf_set][curr_channel]\
                [CONSTANTS['TAG_NAMES']['RX']]
            tx_dict=RF_sets_structure[curr_rf_set][curr_channel]\
                [CONSTANTS['TAG_NAMES']['TX']]

            #get "rf vector"
            rfc_vec_list_rx.append(rx_dict.keys())
            rfc_vec_list_tx.append(tx_dict.keys())

            #validate 3)
            if not islegal_wildcard(rfc_vec_list_rx,len(RFsets)):
                print("Improper use of {0} {1}=-1 wildcard in rx section".format(
                    CONSTANTS['TAG_NAMES']['RFC'],CONSTANTS['ATTRIB_NAMES']['NUM']))
                print("It can only be used with a single-tile board file")
                return False

            if not islegal_wildcard(rfc_vec_list_tx,len(RFsets)):
                print("Improper use of {0} {1}=-1 wildcard in tx section".format(
                    CONSTANTS['TAG_NAMES']['RFC'],CONSTANTS['ATTRIB_NAMES']['NUM']))
                print("It can only be used with a single-tile board file")
                return False

        #validate no. 2)
        if not list_of_identical_elements(rfc_vec_list_rx):
            print("All {0} tags must have the same RFCs defined".format(
                CONSTANTS['TAG_NAMES']['TX']))
            print("This is not the case in RFset {0}".format(curr_rf_set))
            return_value=False

        if not list_of_identical_elements(rfc_vec_list_tx):
            print("All {0} tags must have the same RFCs defined".format(
                CONSTANTS['TAG_NAMES']['RX']))
            print("This is not the case in RFset {0}".format(curr_rf_set))
            return_value=False

    #validate no. 1)
    if not list_of_identical_elements(ch_vec_list):
        print("All Rf sets must have the same channels defined")
        for cnt_rf_set_print in range(0,len(ch_vec_list)):
            print("RFset {0} has channels {1} defined".format(
                cnt_rf_set_print,ch_vec_list[cnt_rf_set_print]))
        return_value=False


    return return_value

def  validate_sector_structure(RF_sets_structure):
    #
    # This function ensures that the numbering of
    # sectors in the *.xml file is legal.
    # Specifically, it ensures
    # 1) all RFCs in same RF set have identical
    #    sector numbering lists
    # 2) Total allowed number of sectors is obeyed
    #

    #easier notation
    CONSTANTS=init_CONSTANTS()
    MAX_SECTORS_SINGLE_TILE=CUSTOMIZATIONS['MAX_SECTORS_SINGLE_TILE']
    MAX_SECTORS_DIVERSITY=CUSTOMIZATIONS['MAX_SECTORS_DIVERSITY']
    MAX_SECTORS_MASSIVE=CONSTANTS['MAX_SECTORS_MASSIVE']

    #determine board file type
    Nrfset=len(RF_sets_structure.keys())
    channel_list=RF_sets_structure[0].keys()
    first_channel=channel_list[0]
    RFCs_per_RFset=[];

    #initialize
    error_msg='';

    for RFset in RF_sets_structure.keys():
        RFCs_per_RFset.append(len(RF_sets_structure[RFset][first_channel]['rx']))

    if (RFCs_per_RFset == [1]):
        board_file_type='single-tile'
    elif (max(RFCs_per_RFset)==1):
        board_file_type='diversity'
        sectors_per_channel=dict.fromkeys(['rx','tx'])
        sectors_per_channel['rx']=dict.fromkeys(channel_list,0)
        sectors_per_channel['tx']=dict.fromkeys(channel_list,0)
    elif len(RFCs_per_RFset)>1:
        board_file_type = 'massive_diversity'
        sectors_per_channel=dict.fromkeys(['rx','tx'])
        sectors_per_channel['rx']=dict.fromkeys(channel_list,0)
        sectors_per_channel['tx']=dict.fromkeys(channel_list,0)
    else:
        board_file_type='massive'

    #constant
    modes=('rx','tx');

    #initialize
    Nsectors=dict.fromkeys(RF_sets_structure.keys())
    for RFset in Nsectors.keys():
        Nsectors[RFset]=dict.fromkeys(channel_list)

        for curr_channel in Nsectors[RFset]:
            Nsectors[RFset][curr_channel]=dict.fromkeys(['rx','tx'])


    #validate 1)
    for RFset in RF_sets_structure.keys():
        for curr_channel in RF_sets_structure[RFset].keys():
            for txrx in modes:
                RFC_vec=RF_sets_structure[RFset][curr_channel][txrx].keys();
                sector_list_list=[];
                for curr_RFC in RFC_vec:
                    sector_list_list.append(RF_sets_structure[RFset]\
                        [curr_channel][txrx][curr_RFC])
                if not RFC_vec and board_file_type == 'massive_diversity':
                    Nsectors[RFset][curr_channel][txrx] = 0
                elif not list_of_identical_elements(sector_list_list):
                    error_msg+="Error in RFset {0}, Channel {1}, {2}: ".format(
                        RFset,curr_channel,txrx)
                    error_msg+="Not all RFCs have identical sector numbers\n"
                else:
                    Nsectors[RFset][curr_channel][txrx]=len(sector_list_list[0])

    #validate 2)
    for RFset in RF_sets_structure.keys():
        for curr_channel in RF_sets_structure[RFset].keys():

            if (board_file_type=='single-tile'):
                if not(Nsectors[RFset][curr_channel]['rx']<=MAX_SECTORS_SINGLE_TILE):
                    error_msg+= \
                        "Single-tile only supports {0} rx sectors (ch{1})\n".format(
                        MAX_SECTORS_SINGLE_TILE,curr_channel)
                if not (Nsectors[RFset][curr_channel]['tx']<=MAX_SECTORS_SINGLE_TILE):
                    error_msg+= \
                        "Single-tile only supports {0} tx sectors (ch{1})\n".format(
                        MAX_SECTORS_SINGLE_TILE,curr_channel)
            if (board_file_type=='massive'):
                if not (Nsectors[RFset][curr_channel]['rx']<=MAX_SECTORS_MASSIVE):
                    error_msg+= \
                        "Massive Array only supports {0} rx sectors (ch{1})\n".format(
                        MAX_SECTORS_MASSIVE,curr_channel)
                if not (Nsectors[RFset][curr_channel]['tx']<=MAX_SECTORS_MASSIVE):
                    error_msg+= \
                        "Massive Array only supports {0} tx sectors (ch{1})\n".format(
                        MAX_SECTORS_MASSIVE,curr_channel)
            if (board_file_type=='diversity'):
                sectors_per_channel['rx'][curr_channel]+=Nsectors[RFset][curr_channel]['rx']
                if not (sectors_per_channel['rx'][curr_channel]<=MAX_SECTORS_DIVERSITY):
                    error_msg+= \
                        "Diversity only supports {0} rx sectors (ch{1})\n".format(
                        MAX_SECTORS_DIVERSITY,curr_channel)
                sectors_per_channel['tx'][curr_channel]+=Nsectors[RFset][curr_channel]['tx']
                if not (sectors_per_channel['tx'][curr_channel]<=MAX_SECTORS_DIVERSITY):
                    error_msg+= \
                        "Diversity only supports {0} tx sectors (ch{1})\n".format(
                        MAX_SECTORS_DIVERSITY,curr_channel)

            if (board_file_type=='massive_diversity'):
                sectors_per_channel['rx'][curr_channel]+=Nsectors[RFset][curr_channel]['rx']
                if not (sectors_per_channel['rx'][curr_channel]<=max(MAX_SECTORS_DIVERSITY,MAX_SECTORS_MASSIVE)):
                    error_msg+= \
                        "Massive Diversity only supports {0} rx sectors (ch{1})\n".format(
                        max(MAX_SECTORS_DIVERSITY,MAX_SECTORS_MASSIVE),curr_channel)
                sectors_per_channel['tx'][curr_channel]+=Nsectors[RFset][curr_channel]['tx']
                if not (sectors_per_channel['tx'][curr_channel]<=max(MAX_SECTORS_DIVERSITY,MAX_SECTORS_MASSIVE)):
                    error_msg+= \
                        "Massive Diversity only supports {0} tx sectors (ch{1})\n".format(
                        max(MAX_SECTORS_DIVERSITY,MAX_SECTORS_MASSIVE),curr_channel)



    assert(error_msg==''),error_msg

    return board_file_type

def  validate_sens_sector_structure(RF_sets_structure):
    #
    # This function ensures that the numbering of
    # sectors in the *.xml file is legal.
    # Specifically, it ensures
    # 1) all RFCs in same RF set have identical
    #    sector numbering lists
    # 2) Total allowed number of sectors is obeyed
    #

    #easier notation
    CONSTANTS=init_CONSTANTS()
    MAX_SECTORS_SENS_TX=CONSTANTS['MAX_SECTORS_SENS_TX']
    MAX_SECTORS_SENS_RX=CONSTANTS['MAX_SECTORS_SENS_RX']

    #determine board file type
    Nrfset=len(RF_sets_structure.keys())
    channel_list=RF_sets_structure[0].keys()
    first_channel=channel_list[0]
    RFCs_per_RFset_tx=[]
    RFCs_per_RFset_rx=[]

    #initialize
    error_msg=''

    for RFset in RF_sets_structure.keys():
        RFCs_per_RFset_rx.append(len(RF_sets_structure[RFset][first_channel]['rx']))
    for RFset in RF_sets_structure.keys():
        RFCs_per_RFset_tx.append(len(RF_sets_structure[RFset][first_channel]['tx']))

    if (Nrfset>1):
        raise ValueError("Currently not supporting more then one pair")
    if (len(RFCs_per_RFset_tx)>1) or (len(RFCs_per_RFset_tx)>1):
        raise ValueError("Currently not supporting more then one TX/RX RF per pair")
    else:
        board_file_type='sensing-params'
        sectors_per_channel=dict.fromkeys(['rx','tx'])
        sectors_per_channel['rx']=dict.fromkeys(channel_list,0)
        sectors_per_channel['tx']=dict.fromkeys(channel_list,0)

    #constant
    modes=('rx','tx');

    #initialize
    Nsectors=dict.fromkeys(RF_sets_structure.keys())
    for RFset in Nsectors.keys():
        Nsectors[RFset]=dict.fromkeys(channel_list)

        for curr_channel in Nsectors[RFset]:
            Nsectors[RFset][curr_channel]=dict.fromkeys(['rx','tx'])

    #validate 1)
    for RFset in RF_sets_structure.keys():
        for curr_channel in RF_sets_structure[RFset].keys():
            for txrx in modes:
                RFC_vec=RF_sets_structure[RFset][curr_channel][txrx].keys();
                sector_list_list=[];
                for curr_RFC in RFC_vec:
                    sector_list_list.append(RF_sets_structure[RFset]\
                        [curr_channel][txrx][curr_RFC])


                if not list_of_identical_elements(sector_list_list):
                    error_msg+="Error in RFset {0}, Channel {1}, {2}: ".format(
                        RFset,curr_channel,txrx)
                    error_msg+="Not all RFCs have identical sector numbers\n"
                Nsectors[RFset][curr_channel][txrx]=len(sector_list_list[0])

    #validate 2)
    for RFset in RF_sets_structure.keys():
        for curr_channel in RF_sets_structure[RFset].keys():
            if (board_file_type=='sensing-params'):
                sectors_per_channel['rx'][curr_channel]+=Nsectors[RFset][curr_channel]['rx']
                if not (sectors_per_channel['rx'][curr_channel]<=MAX_SECTORS_SENS_RX):
                    error_msg+= \
                        "Sensing only supports {0} rx sectors (ch{1})\n".format(
                        MAX_SECTORS_SENS_RX,curr_channel)
                sectors_per_channel['tx'][curr_channel]+=Nsectors[RFset][curr_channel]['tx']
                if not (sectors_per_channel['tx'][curr_channel]<=MAX_SECTORS_SENS_TX):
                    error_msg+= \
                        "Sensing only supports {0} tx sectors (ch{1})\n".format(
                        MAX_SECTORS_SENS_TX,curr_channel)

    assert(error_msg==''),error_msg

    return board_file_type

def generate_RFsets_dict(RF_sets_structure):
    #
    # This function generates the RF sets dictionary, dividing the RF modules
    # into RF sets.  It also validates that no RFC is used in different sets
    #

    CONSTANTS=init_CONSTANTS()

    #initialize validating list
    RFC_already_used=[False]*(1+CONSTANTS['MAX_RFC'])
    first_channel=RF_sets_structure[0].keys()[0]

    #initialize dictionary
    RF_sets_dict=dict.fromkeys(RF_sets_structure.keys())

    #loop over all rf sets
    for curr_rf_set in RF_sets_dict.keys():

        # get "rf vector" for current RFset
        rfc_vec_rx = RF_sets_structure[curr_rf_set][first_channel][CONSTANTS['TAG_NAMES']['RX']].keys()

        rfc_vec_tx = RF_sets_structure[curr_rf_set][first_channel][CONSTANTS['TAG_NAMES']['TX']].keys()

        #compose dictionary
        RF_sets_dict[curr_rf_set]=dict.fromkeys(
            ['RFCs_TX','RFCs_RX','TX_sectors_num','RX_sectors_num'])
        RF_sets_dict[curr_rf_set]['RFCs_TX'] = rfc_vec_tx
        RF_sets_dict[curr_rf_set]['RFCs_RX'] = rfc_vec_rx
        RF_sets_dict[curr_rf_set]['RFCs'] = list(set(RF_sets_dict[curr_rf_set]['RFCs_TX']) | set(RF_sets_dict[curr_rf_set]['RFCs_RX'])) #merge 2 lists

        # check if all the channels have the same the RFCs
        for channel in RF_sets_structure[curr_rf_set]:
            if not rfc_vec_rx==RF_sets_structure[curr_rf_set][channel][CONSTANTS['TAG_NAMES']['RX']].keys():
                print("RFC {0} are in RX sectors of channel {1} and not in channel {2}".format(
                    list(set(rfc_vec_rx)-set(RF_sets_structure[curr_rf_set][channel][CONSTANTS['TAG_NAMES'][
                        'RX']].keys())), first_channel, channel))
                raise ValueError("Illegal input: sectors xml")

            if not rfc_vec_tx==RF_sets_structure[curr_rf_set][channel][CONSTANTS['TAG_NAMES']['TX']].keys():
                print("RFC {0} are in TX sectors of channel {1} and not in channel {2}".format(
                    list(set(rfc_vec_tx)-set(RF_sets_structure[curr_rf_set][channel][CONSTANTS['TAG_NAMES'][
                        'TX']].keys())), first_channel, channel))
                raise ValueError("Illegal input: sectors xml")

    return RF_sets_dict

def generate_RF_sens_sets_dict(RF_sets_structure):
    #
    # This function generates the RF sets dictionary, dividing the RF modules
    # into RF sets.  It also validates that no RFC is used in different sets
    #

    CONSTANTS=init_CONSTANTS()

    #initialize validating list
    RFC_already_used=[False]*(1+CONSTANTS['MAX_RFC'])
    first_channel=RF_sets_structure[0].keys()[0]

    #initialize dictionary
    RF_sens_sets_dict=dict.fromkeys(RF_sets_structure.keys())
    #loop over all rf sets
    for curr_rf_set in RF_sens_sets_dict.keys():

        #get "rf vector" for current RFset
        rfc_rx_vec=RF_sets_structure[curr_rf_set][first_channel]\
            [CONSTANTS['TAG_NAMES']['RX']].keys()
        rfc_tx_vec=RF_sets_structure[curr_rf_set][first_channel]\
            [CONSTANTS['TAG_NAMES']['TX']].keys()
        #compose dictionary
        RF_sens_sets_dict[curr_rf_set]=dict.fromkeys(
            ['TX_RFCs','RX_RFCs','TX_sectors_num','RX_sectors_num'])
        RF_sens_sets_dict[curr_rf_set]['TX_RFCs']=rfc_tx_vec
        RF_sens_sets_dict[curr_rf_set]['RX_RFCs']=rfc_rx_vec
        rx_list =  RF_sets_structure[curr_rf_set][first_channel][CONSTANTS['TAG_NAMES']['RX']]
        tx_list =  RF_sets_structure[curr_rf_set][first_channel][CONSTANTS['TAG_NAMES']['TX']]
        for curr_tx_rfc in tx_list.keys():
            RF_sens_sets_dict[curr_rf_set]['TX_sectors_num'] = len(tx_list.get(curr_tx_rfc))
        for curr_rx_rfc in rx_list.keys():
            RF_sens_sets_dict[curr_rf_set]['RX_sectors_num'] = len(rx_list.get(curr_rx_rfc))
        #this is for validation
        for idx in rfc_rx_vec:
            if RFC_already_used[idx]:
                print("RFC {0} is used in more than one RF set".format(idx))
                raise ValueError("Illegal input: sectors xml")
            else:
                RFC_already_used[idx]=True
    return RF_sens_sets_dict

def generate_scalar_per_rfc(sectors_root,field_name):
    #
    # This function generates a struct for the <field_name> element.
    # This element must have the same structure as the <TPC> element, for example
    # and the actual content must be a scalar per RFC
    #

    CONSTANTS=init_CONSTANTS()
    element_name=CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC'][field_name]['TITLE']

    try:
        CableLossStructure = get_scalar_per_rfc(father_element=sectors_root.find(
            element_name), field_name=field_name)
    except AttributeError: # no <Cable_Loss> tag
        CableLossStructure = get_scalar_per_rfc(all_default_flag=True,
                                                field_name=field_name)
    except Exception as e:
        raise e

    return CableLossStructure



def get_scalar_per_rfc(father_element=None,all_default_flag=False,field_name=None):
    # This function generates a dictionary:
    # keys: 0 to 7 (one key for each RFC)
    # values: scalar for each RFC
    # Any RFC not explicitly mentioned set to its respective default value
    #

    CONSTANTS=init_CONSTANTS()
    error_msg=''

    internal_element_name=CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC'][field_name][
                    'FIELD']

    if all_default_flag:
        #set default
        value_list=set_default_atten_list(field_name)
    else:

        #get rfc vector
        per_RFC_list=specific_labels_in_one_tag(
            father_element,CONSTANTS['TAG_NAMES']['RFC'],
            CONSTANTS['ATTRIB_NAMES']['NUM'])

        #check for wildcard
        if -1 in per_RFC_list:
            #ensure -1 is only RFC
            if len(per_RFC_list)>1:
                error_msg+=("When using RFC=-1 wildcard in <{0}>, ".format(
                    CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC'][field_name][
                    'TITLE'])) + "no other RFC sub-elements may be defined"
            else:
                #process wildcard
                per_RFC_list=range(0,CONSTANTS['MAX_RFC']+1)
                attenuation=float(father_element.find(
                    CONSTANTS['TAG_NAMES']['RFC']).find(internal_element_name).text)
                value_list=[attenuation]*(CONSTANTS['MAX_RFC']+1)
        else:
            #set default
            value_list=set_default_atten_list(field_name)

            #selective overwrite
            for child in father_element:
                #RFC for current child element
                RFC_num=int(child.attrib[CONSTANTS['ATTRIB_NAMES']['NUM']])

                #get required LO power level
                power_level=child.find(internal_element_name).text

                #overwrite previously written default
                value_list[RFC_num]=float(power_level)


    assert(error_msg==''), error_msg

     #initialize output
    OutputStructure=dict.fromkeys(range(0,CONSTANTS['MAX_RFC']+1))
    for rfc in OutputStructure.keys():
        OutputStructure[rfc]=value_list[rfc]

    return OutputStructure

def get_TSSI_calib(TSSI_element,RF_sets_dict):
    # This function generates a dictionary from the
    # <tx_Power_Calibration> tag:
    # keys: channel numbers (not all channels required)
    # values: dictionary as follows:
    #           keys: RFC numbers
    #           values: dictionary as follows:
    #                       keys: 'chain_number','tx_sector'
    # All sanity checks performed in validate_TSSI_label, except for:
    #   1. Ensure RFC not included twice in same channel
    #   2. Ensure no channel is included twice in *.xml file
    #

    CONSTANTS=init_CONSTANTS()
    error_msg=''

    #initialize output structure
    TssiStructure={}

    # find channels defined in xml
    all_channels_list = specific_labels_in_one_tag(TSSI_element, CONSTANTS['TAG_NAMES']['CHANNEL'], CONSTANTS[
            'ATTRIB_NAMES']['NUM'])

    #ensure no channel is included twice in *.xml file
    user_included_channels=[]
    tssi_channels_group_dict = dict()
    for channel_group_element in TSSI_element.findall(CONSTANTS['TAG_NAMES']['CHANNEL']):
        channels_in_group=parse_list_numeric_strings(channel_group_element.get(CONSTANTS['ATTRIB_NAMES'][
            'NUM']))
        assert(len(channels_in_group)>0)
        tssi_channels_group_dict[channels_in_group[0]]=channels_in_group
        for curr_channel in channels_in_group:
            if curr_channel in user_included_channels:
                error_msg+="channel {0} is included more than once in <{1}> element".format(
                    curr_channel,CONSTANTS['TAG_NAMES']['TSSI_CALIB'])
            user_included_channels.append(curr_channel)

    #generate dictionary
    for channel_group_element in TSSI_element.findall(CONSTANTS['TAG_NAMES'][
        'CHANNEL']):
        channels_in_group=parse_list_numeric_strings(channel_group_element.get(
            CONSTANTS['ATTRIB_NAMES']['NUM']))
        for curr_channel in channels_in_group:
            #get rfc vectors for current channel
            rfcs_in_channel_list=specific_labels_in_one_tag(channel_group_element,
                CONSTANTS['TAG_NAMES']['RFC'],CONSTANTS['ATTRIB_NAMES']['NUM'])

            TssiStructure[curr_channel]={}

            for cnt_element in range(0,len(rfcs_in_channel_list)):
                rfc_group_element=channel_group_element[cnt_element]
                RFCs_in_group=parse_list_numeric_strings(rfc_group_element.get(
                    CONSTANTS['ATTRIB_NAMES']['NUM']))
                for curr_rfc in RFCs_in_group:
                    if curr_rfc in TssiStructure[curr_channel].keys():
                        error_msg += "RFC {0} has been included more".format(curr_rfc)
                        error_msg += " than once in channel {0} in".format(curr_channel)
                        error_msg += " <{0}> element\n".format(CONSTANTS['TAG_NAMES']['TSSI_CALIB'])
                    TssiStructure[curr_channel][curr_rfc] = fill_in_TSSI_struct(rfc_group_element)

    if len(user_included_channels) > 0:  # for case TSSI sectors aren't available
        # fill in channels user has not
        nearest_measured_ch=generate_sector_channel_dict(user_included_channels)
       
        for curr_channel in nearest_measured_ch.keys():
            TssiStructure[curr_channel]=copy.deepcopy(TssiStructure[
                nearest_measured_ch[curr_channel]])
            if (curr_channel in CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']) and (curr_channel not in user_included_channels):
                #channel was not measured but we need to group to other channel in order to generate proper sections
                for key in tssi_channels_group_dict.keys():
                    channels_in_group=tssi_channels_group_dict[key]
                    if nearest_measured_ch[curr_channel] in channels_in_group:
                        # add current channel to the group of the closest measured channel
                        tssi_channels_group_dict[key].append(curr_channel)
    
    assert(error_msg==''),error_msg
    
    TssiStructure['ChannelGroups'] = tssi_channels_group_dict

    return TssiStructure

def get_unused_BRP_ants(unused_antenna_element):
    # This function locates all RFC tags
    # and generates a dictionary
    #

    CONSTANTS = init_CONSTANTS()
    error_msg = ''

    # get rfc vector
    unused_ants_RFC_list=specific_labels_in_one_tag(unused_antenna_element, 'RFC', CONSTANTS['ATTRIB_NAMES']['NUM'])

        #check for wildcard
    if -1 in unused_ants_RFC_list:
        #ensure -1 is only RFC
        if len(unused_ants_RFC_list)>1:
            print("When using RFC=-1 wildcard in <{0}>, ".format(
                CONSTANTS['TAG_NAMES']['UNUSED_BRP']) +
                "no other RFC sub-elements may be defined")
            raise ValueError("{0}_tag_wildcard_error".format(
                CONSTANTS['TAG_NAMES']['UNUSED_BRP']))
        else:
            #process wildcard
            unused_ants_RFC_list=range(0,CONSTANTS['MAX_RFC']+1)
            unused_ants=parse_list_numeric_strings(unused_antenna_element.find('RFC').find(
                CONSTANTS['TAG_NAMES']['ANTENNAS']).text)
            unused_ants_list=[unused_ants]*(CONSTANTS['MAX_RFC']+1)
    else:
        #set default
        unused_ants_list=[CONSTANTS['DEFAULT_UNUSED_BRP_ANTENNAS']]*(CONSTANTS['MAX_RFC']+1)

        #selective overwrite
        for child in unused_antenna_element:
            unused_ants_list[int(child.get(CONSTANTS['ATTRIB_NAMES']['NUM']))]=\
            parse_list_numeric_strings(child.find(CONSTANTS['TAG_NAMES']['BRP_ANTENNAS']).text)

    #initialize output dictionary
    UnusedBRPStructure=dict.fromkeys(range(0,CONSTANTS['MAX_RFC']+1))
    for cnt_rfc in UnusedBRPStructure.keys():
        UnusedBRPStructure[cnt_rfc]=CONSTANTS['DEFAULT_UNUSED_BRP_ANTENNAS']

    #convert list to dict
    for rfc in UnusedBRPStructure.keys():
        UnusedBRPStructure[rfc]=unused_ants_list[rfc]

        # FW restriction
        if len(UnusedBRPStructure[rfc]) < 2:
            error_msg += "RFC {0} requires at least 2 unused BRP antennas\n".format(rfc)

    assert(error_msg==''), error_msg

    return UnusedBRPStructure

def generate_BRP_field_parameter(sectors_root,field_name):
    #
    #
    #

    CONSTANTS=init_CONSTANTS()
    element=sectors_root.find(CONSTANTS['TAG_NAMES']['BRP_FIELDS'][field_name])


    if element is None:
        return CONSTANTS['DEFAULT_'+field_name]
    else:
        return int(element.text)
    end

def islegal_wildcard(rfc_vec_list,Nrfsets):
    # This function checks if the usage of
    # the wildcard RFC Num=-1 is legal
    # Input rfc_vec_list has two sub-lists,
    # first for rx, then for tx

    if -1 in rfc_vec_list:
        if Nrfsets>1 or len(rfc_vec_list)>1:
            return False

    return True


def fill_in_TSSI_struct(TSSI_RFC_element):
    #
    #
    #

    CONSTANTS=init_CONSTANTS()
    zz={CONSTANTS['TAG_NAMES']['TSSI_TX_SECTOR'] : Sector(
        xml_input=TSSI_RFC_element.find(CONSTANTS['TAG_NAMES']['SECTOR']), \
        direction='tx'),
        CONSTANTS['TAG_NAMES']['TSSI_TX_CHAIN']  : int(TSSI_RFC_element.find(
            CONSTANTS['TAG_NAMES']['TSSI_TX_CHAIN']).text)}
    return zz

def set_default_atten_list(field_name):
    #
    #
    #

    CONSTANTS=init_CONSTANTS()

    return  [CONSTANTS['DEFAULT_'+field_name]]*(CONSTANTS['MAX_RFC']+1)


def map_dc_source_element(sectors_root):
    #
    # replaces the xml strings "XIF" and "external" with their numeric representation, as defined in CONSTANTS
    #

    CONSTANTS = init_CONSTANTS()

    # check if we have a <DC_Source> element
    dc_source_element = sectors_root.find(CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC']['DC_SOURCE']['TITLE'])
    if not dc_source_element is None:
        rfc_element_list = sectors_root.find(CONSTANTS['TAG_NAMES']['SCALAR_PER_RFC']['DC_SOURCE']['TITLE']).findall(
            'RFC')
        for curr_rfc_element in rfc_element_list:
            curr_rfc_element[0].text = CONSTANTS['DC_SOURCE'][curr_rfc_element[0].text.upper()]

    return sectors_root
