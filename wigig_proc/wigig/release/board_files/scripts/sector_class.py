#-------------------------------------------------------------------------------
# Name:        Sector Class
# Purpose:     Defines class of sector
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

from useful_functions import *

class Sector:

    def __init__(self,xml_input=None,struct_input=None,direction=None):
    #
    # Constructor for Sector class
    #

        #initialize message for potential errors
        error_str=''

        #input check (a)
        if direction is None:
            raise ValueError("Sector cannot be defined without \'direction\' variable")
        elif ((direction.lower()=='tx') or (direction.lower()=='rx')):
            self.direction=direction
        else:
            raise ValueError("Sector \'direction\' variable must be \'rx\' or \'tx\'")

        #input check (b)
        try:
            input_type=self._input_check(xml_input,struct_input)
        except Exception as e:
            print(e.message)
            raise

        CONSTANTS=init_CONSTANTS()

        if input_type=='xml':
            #antennas
            self._process_antennas(xml_input)

            #phases
            try:
                self._process_phases(xml_input)
            except AssertionError as e:
                error_str+="{0} in".format(e.message)

            #etype
            try:
                self._process_etype(xml_input)
            except AssertionError as e:
                error_str+="{0} in".format(e.message)

            #dtype
            try:
                self._process_dtype(xml_input)
            except AssertionError as e:
                error_str+="{0} in".format(e.message)

            #switches
            #no error checking because all possible errors detected by *.xsd
            self.x8=self._process_x8_switches(xml_input)
            self.x16=self._process_x16_switches(xml_input)
            try:
                self.x1_gc_offset=int(xml_input.find(
                    CONSTANTS['TAG_NAMES']['x1_gc_offset']).text)
            except AttributeError:
                self.x1_gc_offset=0
            except:
                raise
        else:
            raise IOError("non-xml input not yet implemented")

        assert(error_str==''), error_str

        return

    def __str__(self):
        #
        # This function prints the sector in ASCII hexadecimal format
        #

        #switch dword
        switch_reg='0x{0:08x}'.format((self.x1_gc_offset*(2**3)+self.x8*(2**8)+
            self.x16*(2**12)))

        #dist dword
        dist_reg='0x{0}'.format(''.join(reversed(map(str,self.Dist_Table))))

        #edge0 dword
        edge0_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: x%2,self.Ant_Etype))))

        #edge1 dword
        edge1_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: (x%4>=2),self.Ant_Etype))))

        #edge2 dword
        edge2_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: (x%8>=4),self.Ant_Etype))))

        #edge3 dword
        edge3_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: (x>=8),self.Ant_Etype))))

        #psh 90 dword
        psh90_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: x%2,self.Ant_Phase))))

        #psh 180 dword
        psh180_reg='0x{0:08x}'.format(list_to_bit_vector(MATLAB_find_boolean_input(
            map(lambda x: x>=2,self.Ant_Phase))))

        return "{0}={1}\n{2}={3}\n{4}={5}\n{6}={7}\n".format(switch_reg,dist_reg,
            edge0_reg,edge1_reg,edge2_reg,edge3_reg,psh90_reg,psh180_reg)


####### Secondary Methods #####################

    def contains(self,antenna_num):
    #
    # returns boolean variable: is antenna_num active in this sector?
    #

        CONSTANTS=init_CONSTANTS()

        return(self.Ant_Etype[antenna_num]>CONSTANTS['DEFAULT_TYPES'][
    'MAX_INACTIVE_ETYPE'])



    @staticmethod
    def _input_check(xml_input,struct_input):
    #
    # This function checks what inputs were
    # passed to the constructor and determines
    # how to process them
    #

        if (xml_input is None) and (struct_input is None):
            method='empty_sector'
        elif (xml_input is not None) and (struct_input is None):
            method='xml'
        elif (xml_input is None) and (struct_input is not None):
            method='struct'
        else:
            raise IOError("Sector constructor cannot receive both xml element and struct inputs")

        return method

    def _process_antennas(self,sector_element):
        #
        # This function build the Ant_32Vec field
        # all verification already done by *.xsd file
        #

        CONSTANTS=init_CONSTANTS()

        #parse input
        antenna_vec=parse_list_numeric_strings(
            sector_element.find(CONSTANTS['TAG_NAMES']['ANTENNAS']).text)

        if (antenna_vec!=sorted(antenna_vec)):
            raise AssertionError("unsorted <antennas> tag")

        if (antenna_vec!=sorted(list(set(antenna_vec)))):
            raise AssertionError(
                "<antennas> tag includes same antenna more than once")

        #initialize output
        self.Ant_32Vec=[False]*CONSTANTS['EDGES_PER_CHIP']

        for antenna in antenna_vec:
            self.Ant_32Vec[antenna]=True

        return

    def _process_phases(self,sector_element):
        #
        # This function builds the phase vector
        # There are two legal cases:
        #   1) the phase vector contains 32 elements
        #   2) the phase vector is the same length as the <antennas> tag
        #

        CONSTANTS=init_CONSTANTS()

        phase_element=sector_element.find(CONSTANTS['TAG_NAMES']['PHASES'])
        phase_vec=parse_list_numeric_strings(phase_element.text)

        #build vector
        if len(phase_vec)==CONSTANTS['EDGES_PER_CHIP']:
            self.Ant_Phase=phase_vec
        elif (len(phase_vec)==sum(self.Ant_32Vec)):
            self.Ant_Phase=self._fill_vector_by_antenna_index(self.Ant_32Vec,
                CONSTANTS['EDGES_PER_CHIP'],phase_vec,0) #zero is default psh value
        else:
            raise AssertionError("phase vector length unequal to antenna vector length")

        return


    def _process_etype(self,sector_element):
        #
        # This function builds the etype vector.  Note that it modifies the
        # Ant_Etype attribute.  It also performs two sanity checks on the
        # <antennas> element
        # 1) making sure that the EType for all active
        #    antennas is at least 2.
        # 2) making sure that the EType for all inactive
        #    antennas is at most 1.
        #

        CONSTANTS=init_CONSTANTS()
        Nant=sum(self.Ant_32Vec)

        error_msg=''

        #read etype data from *.xml file
        etype_vec,method=self._read_type_vector_tag(sector_element,'edge')

        #get default types
        default_inactive_type,default_active_type,default_equalization_type=\
            self._get_default_types('edge')

        if (method=='undefined'):
            #all active edges set to default type
            etype_vec=[default_active_type]*Nant
            self.Ant_Etype=self._fill_vector_by_antenna_index(
                self.Ant_32Vec,CONSTANTS['EDGES_PER_CHIP'],etype_vec,default_inactive_type)
            self._equalize('edge')
        elif (method=="full"):
            self.Ant_Etype=etype_vec
        elif (method=="specific"):
            assert(len(etype_vec)==Nant),\
                "Etype vector length unequal to antenna vector length"
            self.Ant_Etype=self._fill_vector_by_antenna_index(
                self.Ant_32Vec,CONSTANTS['EDGES_PER_CHIP'],etype_vec,default_inactive_type)
            self._equalize('edge')
        else:
            raise ValueError("funtion Sector._read_type_vector_tag returned \
                illegal method variable")

        #sanity check on <ETypes> element
        for cnt_ant in range(0,len(self.Ant_Etype)):
            #ensure user has not tried to give EType>1 to an active antenna
            if (not self.Ant_32Vec[cnt_ant]) and (
                self.Ant_Etype[cnt_ant]>default_equalization_type):

                error_msg+=('Antenna {0} is not listed as active, so its EType ' +
                    'must be at most {1}\n').format(
                    cnt_ant,default_equalization_type)

            #ensure user has not tried to give EType<=1 to an active antenna
            if (self.Ant_32Vec[cnt_ant]) and (
                self.Ant_Etype[cnt_ant]<=default_equalization_type):

                error_msg+=('Antenna {0} is listed as active, so its EType ' +
                    'must be at least {1}\n').format(
                    cnt_ant,1+default_equalization_type)

        assert(error_msg==''),error_msg

        return



    def _process_dtype(self,sector_element):
        #
        # This function builds the dtype vector
        #

        CONSTANTS=init_CONSTANTS()

        dtype_vec,method=self._read_type_vector_tag(sector_element,'dist')

        if (method=='undefined'):
            self._build_dists_from_empty()
        elif (method=="full"):
            self.Dist_Table=dtype_vec
        elif (method=="specific"):
            try:
                self._build_dists_from_specific(dtype_vec)
            except AssertionError as e:
                raise
        else:
            raise ValueError("funtion Sector._read_type_vector_tag returned \
                illegal method variable")

        return

    def _process_x8_switches(self,sector_element):
        #
        # This function builds the x8 value
        #

        CONSTANTS=init_CONSTANTS()

        x8_element=sector_element.find(CONSTANTS['TAG_NAMES']['x8'])

        if x8_element is None: #user did not define
            if (self.direction=='rx'):
                #find active dists
                binary_dvec=map(lambda x,y: x>y,self.Dist_Table,
                    [CONSTANTS['DEFAULT_TYPES']['MAX_INACTIVE_DTYPE']]*len(
                    self.Dist_Table))

                #initialize switch vector
                x8_vector=[False]*(CONSTANTS['DISTS_PER_CHIP']/2)

                for cnt_swch in range(0,CONSTANTS['DISTS_PER_CHIP']/2):
                    #easier notation
                    low_idx=cnt_swch*2
                    high_idx=low_idx+1

                    #set swch
                    x8_vector[cnt_swch]=(binary_dvec[low_idx] or binary_dvec[high_idx])

                #equalize
                if sum(x8_vector)==(CONSTANTS['DISTS_PER_CHIP']/2-1): #3 swchs out of 4
                    x8_vector=[True]*(CONSTANTS['DISTS_PER_CHIP']/2)
            elif (self.direction=='tx'):
                x8_vector=[1,1,1,1]
            else:
                raise ValueError(
                    "Sector \'direction\' variable must be \'rx\' or \'tx\'")

        else: #user defined
            x8_vector=parse_list_numeric_strings(x8_element.text)

        #bin2dec
        x8_value=sum(map(lambda x,y: x*y,x8_vector,[1,2,4,8]))

        return x8_value

    def _process_x16_switches(self,sector_element):
        #
        # This function builds the x16 value
        #

        CONSTANTS=init_CONSTANTS()

        x16_element=sector_element.find(CONSTANTS['TAG_NAMES']['x16'])

        if x16_element is None: #user did not define
            if (self.direction=='rx'):
                x16_vector=[False]*2
                x16_vector[0]=any(self.Ant_32Vec[0:CONSTANTS['EDGES_PER_CHIP']/2])
                x16_vector[1]=any(self.Ant_32Vec[CONSTANTS['EDGES_PER_CHIP']/2:\
                    CONSTANTS['EDGES_PER_CHIP']])
            elif (self.direction=='tx'):
                x16_vector=[1,1]
            else:
                raise ValueError(
                    "Sector \'direction\' variable must be \'rx\' or \'tx\'")

        else:
            x16_vector=parse_list_numeric_strings(x16_element.text)

        #bin2dec
        x16_value=sum(map(lambda x,y: x*y,x16_vector,[1,2]))

        return x16_value


######### Tertiary Methods ####################

    def _read_type_vector_tag(self,sector_element,amp_name):
        #
        # This is a helper function to extract a dtype or etype
        # element from the xml, and also to determine if the user
        # has 1) not defined a type vector OR
        #     2) defined a type for all 32(8) edges(dists) OR
        #     3) only defined a type for those amps used in sector
        #

        CONSTANTS=init_CONSTANTS()

        if amp_name.lower()=='edge':
            tag_name=CONSTANTS['TAG_NAMES']['ETYPES']
            full_length=CONSTANTS['EDGES_PER_CHIP']
        elif amp_name.lower()=='dist':
            tag_name=CONSTANTS['TAG_NAMES']['DTYPES']
            full_length=CONSTANTS['DISTS_PER_CHIP']
        else:
            raise ValueError("funtion Sector._read_type_vector_tag called illegally")

        #get type element from xml element
        try:
            type_vec=parse_list_numeric_strings(sector_element.find(tag_name).text)
        except AttributeError as e:
            type_vec=None

        if type_vec is None:
            method="undefined"
        elif len(type_vec)==full_length:
            method="full"
        else:
            method="specific"

        return type_vec,method

    def _build_dists_from_empty(self):
        #
        # This function fills in the dist vector when no <DType>
        # tag is supplied by the user.  It reads the active antennas,
        # activates the corresponding dists, and, for rx, equalizes.
        #

        CONSTANTS=init_CONSTANTS()

        #get default types
        default_inactive_type,default_active_type,default_equalization_type=\
            self._get_default_types('dist')

        #get output
        self.Dist_Table=self._fill_dist_vector(default_inactive_type,default_active_type)

        #equalize
        self._equalize('dist')

        return

    def _build_dists_from_specific(self,dtype_vec):
        #
        # This function is called when the user supplies a <DTypes> tag
        #

        CONSTANTS=init_CONSTANTS()

        #get default types
        default_inactive_type,default_active_type,default_equalization_type=\
            self._get_default_types('dist')

        #initialize output
        self.Dist_Table=[default_inactive_type]*CONSTANTS['DISTS_PER_CHIP']

        #how many dists should have been supplied by user?
        dist_required=self._fill_dist_vector(False,True)

        assert(sum(dist_required)==len(dtype_vec)),\
            "Supplied {0} dtype values, but {1} are required".format(
            len(dtype_vec),sum(dist_required))

        self.Dist_Table=self._fill_vector_by_antenna_index(dist_required,
            CONSTANTS['DISTS_PER_CHIP'],dtype_vec,default_inactive_type)


        #equalize
        self._equalize('dist')

        return


    def _get_default_types(self,amp_name):
        #
        # This function only reads values from the CONSTANTS dictionary
        #

        CONSTANTS=init_CONSTANTS()

        #set default_type
        if (amp_name.lower()=='edge'):
            rx_default=CONSTANTS['DEFAULT_TYPES']['RX_ETYPE']
            tx_default=CONSTANTS['DEFAULT_TYPES']['TX_ETYPE']
            default_equalization_type=CONSTANTS['DEFAULT_TYPES']['MAX_INACTIVE_ETYPE']
        elif (amp_name.lower()=='dist'):
            rx_default=CONSTANTS['DEFAULT_TYPES']['RX_DTYPE']
            tx_default=CONSTANTS['DEFAULT_TYPES']['TX_DTYPE']
            default_equalization_type=CONSTANTS['DEFAULT_TYPES']['MAX_INACTIVE_DTYPE']
        else:
            raise ValueError(" illegal amp_name in Sector._get_default_types")

        if (self.direction=='tx'):
            default_inactive_type=CONSTANTS['DEFAULT_TYPES']['MAX_INACTIVE_ETYPE']
            default_active_type=tx_default
        elif (self.direction=='rx'):
            default_inactive_type=CONSTANTS['DEFAULT_TYPES']['PWDN_TYPE']

            default_active_type=rx_default
        else:
            raise ValueError("illegal self.direction in Sector._get_default_types")

        return default_inactive_type,default_active_type,default_equalization_type

    @staticmethod
    def _fill_vector_by_antenna_index(bool_placement_vector
        ,Nant,inp_vector,default_value):
        #
        # This function spreads out the values in inp_vector over the True
        # entries in bool_placement_vector.  All other entries are assigned
        # default_value
        #

        assert(sum(bool_placement_vector)==len(inp_vector)),\
            "illegal call to _fill_vector_by_antenna_index"
        #initialize output
        out_vector=[default_value]*Nant

        cnt_occurence=0;
        for cnt_ant in range(0,Nant):
            if bool_placement_vector[cnt_ant]:
                out_vector[cnt_ant]=inp_vector[cnt_occurence]
                cnt_occurence+=1;

        return out_vector

    def _fill_dist_vector(self,default_inactive_type,default_active_type):
        #
        # This function is called whether or not the user supplies the
        # <DTypes> tag.  If s/he does, it is called to determine which dists are
        # to be assigned the values in the tag (boolean output).  Otherwise,
        # this function actually assigns the DType values to be used in the
        # sector
        #

        CONSTANTS=init_CONSTANTS()

        #initialize output
        output_list=[]

        for cnt_dist in range(0,CONSTANTS['DISTS_PER_CHIP']):
            #easier notation
            low_idx=CONSTANTS['EDGES_PER_DIST']*cnt_dist
            high_idx=CONSTANTS['EDGES_PER_DIST']*(cnt_dist+1)

            is_active=sum(self.Ant_32Vec[low_idx:high_idx])>0

            output_list.append(default_active_type*(is_active)+
                default_inactive_type*(not is_active))

        return output_list

    def _equalize(self,amp_name):
        #
        # Switch equalization
        #

        CONSTANTS=init_CONSTANTS();
        default_inactive_type,default_active_type,default_equalization_type=\
            self._get_default_types(amp_name)

        #intialize
        if (amp_name.lower()=='edge'):
            on_in_subdivision=[0]*CONSTANTS['DISTS_PER_CHIP']
            scanned_vector=self.Ant_Etype #shallow  copy
            subdivision_size=CONSTANTS['EDGES_PER_DIST']
        elif (amp_name.lower()=='dist'):
            subdivision_size=CONSTANTS['DISTS_PER_SPDT']
            on_in_subdivision=[0]*(CONSTANTS['DISTS_PER_CHIP']/subdivision_size)
            scanned_vector=self.Dist_Table #shallow copy

        else:
            raise AssertionError(
                "illegal call to Sector.__find_max_per_subdivision")
        Nconduct_in_subdivision=[0]*(len(scanned_vector)/subdivision_size)

        #get MAX num of active amplifiers in each sub-division
        for cnt_subdiv in range(0,len(on_in_subdivision)):
            low_idx=cnt_subdiv*subdivision_size
            high_idx=(cnt_subdiv+1)*subdivision_size
            on_in_subdivision[cnt_subdiv]=sum(map(
                lambda x: x>default_equalization_type,scanned_vector[low_idx:high_idx]))
            Nconduct_in_subdivision[cnt_subdiv]=sum(map(
                lambda x: x>=default_equalization_type,scanned_vector[low_idx:high_idx]))
        max_on=max(on_in_subdivision)

        #determine how many additional switches need to conduct
        need_to_add=map(lambda x,y,z:(x-y)*z,[max_on]*len(Nconduct_in_subdivision),
                Nconduct_in_subdivision,map(lambda x: x>0,Nconduct_in_subdivision))

        #set required switches to conducting
        for cnt_subdiv in range(0,len(on_in_subdivision)):
            #easier notation
            low_idx=cnt_subdiv*subdivision_size
            high_idx=(cnt_subdiv+1)*subdivision_size

            #find candidates to be conducted
            off=map(lambda x: x<default_equalization_type,
                    scanned_vector[low_idx:high_idx])
            index_list=MATLAB_find_boolean_input(off)

            #conduct switches
            for cnt in range(0,need_to_add[cnt_subdiv]):
                scanned_vector[low_idx+index_list[cnt]]=default_equalization_type

        return

