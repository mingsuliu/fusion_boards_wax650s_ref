#-------------------------------------------------------------------------------
# Name:        validate_TSSI_label
# Purpose:     Ensures consistency of TSSI label with sectors
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

from useful_functions import *
from customizations import *
from sector_class import Sector
import xml.etree.ElementTree as ET


############### Called from Main function ###############


def post_process(dark_blue_output):
    #
    # 1. Ensure consistency of TSSI label
    # 2. Make sure sectors 38 and 68 are not being used in diversity mode
    #

    error_msg=''

    try:
        validate_wildcard(dark_blue_output)
    except Exception as e:
        error_msg+=e.message
        raise AssertionError(error_msg)

    try:
        TSSI_label_post_process(dark_blue_output)
    except Exception as e:
        error_msg+=e.message

    #must come after validate_TSSI_label
    if dark_blue_output['BoardFileType'].lower()=='diversity':
        try:
            check_OTP_bug_in_diversity(dark_blue_output)
        except Exception as e:
            error_msg+=e.message

        try:
            check_diversity_sector_numbers(dark_blue_output)
        except Exception as e:
            error_msg+=e.message

    if error_msg=='':
        return dark_blue_output
    else:
        raise AssertionError(error_msg)



################## Wildcard #############################

def validate_wildcard(dark_blue_output):
    #
    # This function ensures that, if the RFC=-1 wilcard is used in the <RFsets>
    # element, there is no ambiguity with other labels, that are listed in
    #

    CONSTANTS=init_CONSTANTS()
    error_msg=''

    #check if wildcard is used
    channels=dark_blue_output['Sectors'].keys()
    if (dark_blue_output['Sectors'][channels[0]]['rx'].keys==-1):

        double_check_fields=[CONSTANTS['INTERNAL']['TSSI_STRUCT'],
            CONSTANTS['INTERNAL']['BRP_STRUCT'],
            CONSTANTS['INTERNAL']['CABLE_STRUCT']]

        for field in double_check_fields:
            if not check_identical_lists(dark_blue_output['GP_Labels'][field]):
                error_msg+= \
                    'when using RFC=-1 wildcard, all <RFC> elements inside <{0}> must be identical\n'.format(
                    translate_struct_to_xml_tag(field))

        assert(error_msg==''),(error_msg + \
                'This can be achieved by using the wildcard in these fields')


############## TSSI Label ###############################

def TSSI_label_post_process(dark_blue_output):
    #
    # 1. Ensure TSSI label data does not contradict sectors
    # 2. generate rx switches only sectors
    #

    try:
        validate_TSSI_label(dark_blue_output)
    except Exception as e:
        raise

    try:
        dark_blue_output=add_rx_switches_sectors(dark_blue_output)
    except Exception as e:
        raise

    return dark_blue_output


def validate_TSSI_label(dark_blue_output):
    #
    # 4. Ensure TSSI label is self-consistent.
    # 1. Ensure RFC=-1 is not used in conjunction with other RFCs
    # 2. If user used -1 in sectors area, he must use -1 in this label
    # 3. Ensure every RFC in <RFsets> element has a TSSI element, for each channel
    #

    error_msg=''

    CONSTANTS=init_CONSTANTS()

    #easier notation
    TSSICalibStructure=CONSTANTS['INTERNAL']['TSSI_STRUCT']

    #Ensure RFC=-1 is not used in conjunction with other RFCs
    for channel in dark_blue_output['GP_Labels'][TSSICalibStructure]:
        if channel in CONSTANTS['SUPPORTED_CHANNELS']:
            RFCs_in_channel=dark_blue_output['GP_Labels'][TSSICalibStructure][
                channel].keys()
            if (-1 in RFCs_in_channel) and (len(RFCs_in_channel)>1):
                error_msg+="{0} element contains RFC=-1 and other RFCs for ".format(
                    CONSTANTS['TAG_NAMES']['TSSI_CALIB'])
                error_msg+="at least one channel\n"
                break

    #If user used -1 in sectors area, he must use -1 in this label
    for channel in dark_blue_output['Sectors']:
        for RFC in dark_blue_output['Sectors'][channel]['tx']:
            #if we have reached here, the function validate_wildcard passed
            # successfully
            if (RFC==-1) and (dark_blue_output['GP_Labels'][TSSICalibStructure][
                channel].keys()!=[-1]): #this is illegal
                error_msg+="In channel {0}, RFC=-1 was used in the <{1}>".format(
                    channel,CONSTANTS['TAG_NAMES']['RF_SETS'])
                error_msg+=" element.  Therefore, you must use the RFC=-1 "
                error_msg+="wildcard in the <{0}> element for this channel\n".format(
                    CONSTANTS['TAG_NAMES']['TSSI_CALIB'])

    assert(error_msg==''),error_msg

    if dark_blue_output['GP_Labels'][TSSICalibStructure]['ChannelGroups'] != {}:
        #Ensure each RFC has a TSSI element, for each channel
        for channel in dark_blue_output['Sectors']:
            #easier notation
            RFCs_in_TSSI_struct_this_channel=dark_blue_output['GP_Labels'][ 
                TSSICalibStructure][channel].keys()

            for RFC in dark_blue_output['Sectors'][channel]['tx']:
                if not (RFC in RFCs_in_TSSI_struct_this_channel) and (
                    RFCs_in_TSSI_struct_this_channel!=[-1]):

                    error_msg+="In channel {0}, RFC {1} does not have a".format(
                        channel,RFC)
                    error_msg+=" definition in the <{0}> element\n".format(
                        CONSTANTS['TAG_NAMES']['RF_SETS'])


    #Ensure TSSI label is self-consistent.
    for channel in dark_blue_output['GP_Labels'][TSSICalibStructure]:
        if channel in CONSTANTS['SUPPORTED_CHANNELS']:
            RFCs_in_channel=dark_blue_output['GP_Labels'][TSSICalibStructure][
                channel].keys()
            for RFC in RFCs_in_channel:
                #read TSSI element for current RFC
                TSSI_antenna=dark_blue_output[
                    'GP_Labels'][TSSICalibStructure][channel][RFC][
                    CONSTANTS['TAG_NAMES']['TSSI_TX_CHAIN']]
                TSSI_sector=dark_blue_output[
                    'GP_Labels'][TSSICalibStructure][channel][RFC][
                    CONSTANTS['TAG_NAMES']['TSSI_TX_SECTOR']]

                #ensure pointed sector contains listed antenna
                if not (TSSI_sector.contains(TSSI_antenna)):
                    error_msg+="TSSI sector for RFC{0} in channel {1} ".format(RFC,
                        channel)
                    error_msg+="does not contain TSSI antenna ({0})\n".format(
                        TSSI_antenna)

    assert(error_msg==''),error_msg

    return

def add_rx_switches_sectors(dark_blue_output):
    #
    # Adds the x8 switches-only sector at the first empty slot
    #

    CONSTANTS=init_CONSTANTS()

    #generate new sector (rx switches sector)
    new_sector=ET.Element('sector')
    new_sector.append(ET.Element('antennas'))
    new_sector.append(ET.Element('Phases'))
    x8_element=ET.Element('x8')
    x8_element.text='1 1 1 1'
    new_sector.append(x8_element)
    x16_element=ET.Element('x16')
    x16_element.text='0 0'
    new_sector.append(x16_element)
    x1_gc_offset=ET.Element('x1_gc_offset')
    x1_gc_offset.text='0'
    new_sector.append(x1_gc_offset)

    #initialize
    legal_number=False;
    error_msg=''
    MAX_RX_SECTOR_VALUE=999

    #determine required location of rx switches sector
    if  None == CUSTOMIZATIONS['SWITCHS_RX_SECTOR']: #we need to calculate rx switch sector location
        #initialize internal variable
        all_possibilities_list=[True]*CONSTANTS['SECTOR_TABLE_SIZE']
        for curr_sector in CONSTANTS['ILLEGAL_SECTORS']:
            all_possibilities_list[curr_sector]=False

        #rule out already-used sector numbers
        for channel in dark_blue_output['Sectors']:
            for RFC in dark_blue_output['Sectors'][channel]['rx']:
                for curr_sector in dark_blue_output['Sectors'][channel]['rx'][
                    RFC].keys():

                    all_possibilities_list[curr_sector]=False

        for cnt in range (0,len(all_possibilities_list)):
            if all_possibilities_list[cnt]:
                MAX_RX_SECTOR_VALUE=cnt
                legal_number=True;
                break

        if not legal_number: #we have a problem
            error_msg+="No legal slot is available for the RF switches sector."
            error_msg+=" See xml tool documentation for further details"

    else: #rx switch sector location is dictated by FW version
        MAX_RX_SECTOR_VALUE=CUSTOMIZATIONS['SWITCHS_RX_SECTOR']

    #if we have an error, no need to continue
    assert(error_msg==''),error_msg

    #add pointer to new sector to TSSI label structure
    dark_blue_output['GP_Labels'][CONSTANTS['INTERNAL']['TSSI_STRUCT']][
        'rx_sector']=MAX_RX_SECTOR_VALUE

    #add rx switches sector to dark_blue_output, for each RFC for each channel
    new_sector.set('Num',MAX_RX_SECTOR_VALUE)
    for channel in dark_blue_output['Sectors']:
        for RFC in dark_blue_output['Sectors'][channel]['rx']:
            dark_blue_output['Sectors'][channel]['rx'][RFC][
                MAX_RX_SECTOR_VALUE]=Sector(xml_input=new_sector,direction='rx')

    return dark_blue_output

############## Sector 38,68 check ###############################

def check_OTP_bug_in_diversity(dark_blue_output):
    #
    # This function ensures sectors 38 and 68 are not being used.  It is called
    # only for a diversity board file.  It must be called after
    # add_rx_switches_sectors
    #

    CONSTANTS=init_CONSTANTS()

    error_msg=''

    for channel in dark_blue_output['Sectors'].keys():
        for mode in ('rx','tx'):
            #sort all active RFCs in current channel
            RFCs=sorted(dark_blue_output['Sectors'][channel][mode].keys())

            if mode=='tx':
                #number of sectors fw allocates to each RFC
                sector_offset=int(CONSTANTS[
                    'FW_CONST_NUM_DIVERSITY_SECTORS']/len(RFCs))
            elif mode=='rx':
                #in rx, there is no "block diagonal" sector table, because the
                #omni sector is activated like Massive Array
                sector_offset=0
            else:
                raise AssertionError

            #initialize counter
            sector_counter=0

            #initialize empty list
            #This list will continuously number all sectors in current channel
            sector_list=[]

            for curr_RFC in RFCs:
                curr_sector_numbers=sorted(dark_blue_output['Sectors'][channel] \
                    [mode][curr_RFC].keys())
                num_sectors=len(curr_sector_numbers)
                sector_list=sector_list+(map(lambda x,y: x+y,curr_sector_numbers,
                    [sector_counter]*len(curr_sector_numbers)))
                if CUSTOMIZATIONS['CONCATE_DIVERSITY_SECTORS'] == False:
                    sector_counter+=sector_offset

            for illegal_sector in CONSTANTS['ILLEGAL_SECTORS']:
                if illegal_sector in sector_list:
                    error_msg+= \
                        'Illegal diversity sector {0} in channel {1}, {2}\n'.format(
                            illegal_sector,channel,mode)

    assert(error_msg==''),error_msg

def check_diversity_sector_numbers(dark_blue_output):
    #
    # Ensure sector numbers are not larger than 64/N
    #

    error_msg=''
    MAX_DIVERSITY_SECTORS=1+CUSTOMIZATIONS['MAX_SECTORS_DIVERSITY']

    #find order of diversity
    channels=dark_blue_output['Sectors'].keys()
    first_channel=channels[0] #we can do this because we have already validated all channels have same RFCs
    N=len(dark_blue_output['Sectors'][first_channel]['tx'].keys())

    #maximal sector number
    max_sec_num=int(MAX_DIVERSITY_SECTORS/N) #round down

    for channel in dark_blue_output['Sectors'].keys():
        for mode in ('tx','rx'):
            for RFC in dark_blue_output['Sectors'][channel][mode].keys():
                if not (max(
                    dark_blue_output['Sectors'][channel][mode][RFC].keys()) <
                    max_sec_num):
                        error_msg+="For diversity-{0}, max sector number is {1}. ".format(
                            N,max_sec_num-1)
                        error_msg+="However, in channel {0}, {1}, RFC {2}, this ".format(
                            channel,mode,RFC)
                        error_msg+="condition is unfulfilled\n"

    assert(error_msg==''),error_msg



def check_identical_lists(input_dict):
    #
    # This function assumes input is a dictionary with all keys from 0 to
    # MAX_RFC.  It then checks that the values for all of these keys are
    # identical
    #

    CONSTANTS=init_CONSTANTS()

    big_list=[];

    for RFC in range (0,CONSTANTS['MAX_RFC']+1):
        big_list.append(input_dict[RFC])

    return list_of_identical_elements(big_list)
