#-------------------------------------------------------------------------------
# Name:        gen_ini_output module
# Purpose:
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#  Includes
#-------------------------------------------------------------------------------
import xml.etree.ElementTree as ET
from useful_functions import *
from customizations import *
from sectors import *

#-------------------------------------------------------------------------------
#  Globals
#-------------------------------------------------------------------------------
GENERIC_CONSTANTS = init_CONSTANTS()


GEN_INI_CONSTANTS = {
        'BOARD_FILE_FORMAT_VERSION_VP': 0xBDF0BDF0,
        'BOARD_FILE_FORMAT_SUPPORTED_VERSION': 0x50000000,
        'BOARD_FILE_FORMAT_TEXT_DESCRIPTION': ';; Talyn multi-array format',
        'COMMON_RF_SECTION_VP' : 0xB000900D,
        'PARAMETERS_SECTION_VP' : 0xA000900D,
        'TSSI_INFORMATION_LABEL_ADDR' : 0x8fffbb, 
        'XIFS_BIAS_DEFINITION_LABEL_ADDR' : 0x8ffff2,
        'RF_TX_TABLE_BRD_LABEL_ADDR' : 0x8ffff5,
        'RF_RX_G0_TABLE_BRD_LABEL_ADDR' : 0x8ffff6,
        'RF_RX_G1_TABLE_BRD_LABEL_ADDR' : 0x8ffff7,
        'RF_TX_SENS_SECTOR_CONFIG_BRD_LABEL_ADDR' : 0x8ffffb, #deprecated
        'TPC_MAX_ALLOWED_POWER_BRD_LABEL_ADDR' : 0x8ffffc,
        'LO_BB_POWER_LIMIT_BRD_LABEL_ADDR': 0x8ffffd,
        'RF_SETS_INFO_LABEL_V1' : 0x8ffffe,
        'RF_SETS_INFO_LABEL_V2' : 0x8ffffa,
        'RF_SENS_SETS_INFO_LABEL' : 0x8fffe0,
        'VALID_ANTENNAS_BRD_LABEL_ADDR' : 0x8fffff,
        'ID_REG_ADDRESS' : 0x880014,
        'FW_VERSION_LABEL_ADDR' : 0xFEDCBA98, #invalid address
        'ID_SENS_REG_ADDRESS' : 0x880024,
        'NUM_OF_GENERAL_SECTIONS' : 2, #RF config and General Parameters
        'BYTES_PER_DWORD' : 4,
        'DWORDS_PER_LINE' : 2,
        'LINES_PER_SECTOR' : 4,
        'DEFAULT_SECTOR_MASK' : 0x3F,
        'DEFAULT_RF_BASE_ADDRESS' : 0xDEADBEEF,
        'BOARD_FILE_FORMAT_VERSION_SENS_VP' : 0xBDF2BDF2,
        'NUMBER_OF_MODES_SENS' : 4, # Search, Face, Gesture, Test
        'MAX_NUM_RF_SETS' : 4, # Maximum allowed number of RF_Sets
        'ENABLED_SENS_MODE' : 0x0000800E,
        'SUBTASK_MASK_SENS' : 0x0000001F  #         subtask mask for TXSS, CALIB, PULSE, SWITCH,and RXON,
    }
    
    
HW_DEFINITIONS = {'Talyn': {'TX_SECTOR_ADDRESS': 0x1000, 'RX_SECTOR_ADDRESS': 0x2000, 'SILICON_ID': 3},
                  'Borrelly': {'TX_SECTOR_ADDRESS': 0x1000, 'RX_SECTOR_ADDRESS': 0x2000, 'SILICON_ID': 4},}
    

#-------------------------------------------------------------------------------
#  Classes
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class BitFieldDW:

    def __init__(self,start_bit,len,val):
        val &= BitFieldDW.bit_mask(len)
        self.dw = (val << start_bit)
        
    @classmethod
    def bit_mask(cls, len):
        return ((2 << (len-1))-1)

    def add_field(self,start_bit,len,val):
        val &= BitFieldDW.bit_mask(len)
        self.dw |= (val << start_bit)

    # def add_field(self,other):
    #     self.dw |= int(other)

    def __add__(self, other):
        self.dw |= int(other)

    def __int__(self):
        return self.dw

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionIni:
    def __init__(self, dark_blue_output):
        self.output_str = ''
        self.line_count = 0
        self.output_str_data = ''
        self.line_count_data = 0
        self.channel_group_list = []
        self.byte_offset = 0

    def generate_section_header(self,
                                description_str,
                                validation_pattern,
                                section_type_val,
                                data_type_val,
                                rf_vector,
                                num_of_data_lines,
                                sector_mask=GEN_INI_CONSTANTS['DEFAULT_SECTOR_MASK'],
                                rf_base_address=GEN_INI_CONSTANTS['DEFAULT_RF_BASE_ADDRESS']):
        output_str = ''
        line_count = 0

        output_str += ("\n;;;;;;;;;;;;;;;%s;;;;;;;;;;;;;;;;;;;;\n" % (description_str))
        output_str += (";; sub section validity:\n")
        output_str += write_ini_line(validation_pattern, validation_pattern, '')
        line_count += 1

        output_str += ("\n;;section header\n")
        data_length_dw = (num_of_data_lines) * GEN_INI_CONSTANTS['DWORDS_PER_LINE']

        #         struct
        #         {
        #             U32 rf_vector : 8;          //Active bit per active RF. If all bits are inactive: broadcast the data
        #                                         //to all RFS as configured in the Query flow (FW).
        #             U32 block_data_length_dw : 12;    //size of the data block of every RF array [in U32 dwords]
        #             U32 section_type : 4;        //0 - CH RX; 1 - CH TX; 2 - RF common; 3 - GeneralParameters/Baseband
        #             U32 data_format : 2;        //0 - {addr/data}; 1 - {base addr + number of words}
        #             U32 sector_mask : 6;        //Valid only on data_format==1
        #                                         //On the 6 bits vector: Active bits('1'): write next word and increment address counter
        #                                         //Inactive bit('0'): only increment of address counter LSB first
        #             U32 rf_base_address;        //Relevant only for data_format==1 , ignore otherwise
        #         } fields;
        bfdw = BitFieldDW(0, 8, rf_vector)
        bfdw.add_field(8, 12, data_length_dw)
        bfdw.add_field(20, 4, section_type_val)
        bfdw.add_field(24, 2, data_type_val)
        bfdw.add_field(26, 6, sector_mask)
        section_header_dw = int(bfdw)

        output_str += write_ini_line(section_header_dw, rf_base_address, '')
        line_count += 1

        data_type_string = ''
        if data_type_val == 0:
            data_type_string = 'addr/data'
        else:
            data_type_string = 'data only'

        output_str += ("\n;;Data format: %s\n\n" % (data_type_string))

        return output_str, line_count

    def __str__(self):
        return self.output_str

    def get_line_count(self):
        return self.line_count

    def get_dword_length(self):
        return self.line_count * GEN_INI_CONSTANTS['DWORDS_PER_LINE']

    def get_byte_length(self):
        return self.get_dword_length() * GEN_INI_CONSTANTS['BYTES_PER_DWORD']

    def is_channel_in_channel_group_list(self, ch):
        
        if ch in self.channel_group_list:
            return True
        else:
            return False

    def set_byte_offset(self, byte_offset):
        self.byte_offset = byte_offset


    def get_byte_offset(self):
        return self.byte_offset

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionIniCommonRf(SectionIni):
    def __init__(self, dark_blue_output):
        SectionIni.__init__(self, dark_blue_output)

        self.output_str_data, self.line_count_data = generate_rf_config_section(dark_blue_output, [])#no channel group
        
        self.output_str, self.line_count = SectionIni.generate_section_header(self,
                                                                              'Common RF Section',
                                                                              GEN_INI_CONSTANTS['COMMON_RF_SECTION_VP'],
                                                                              2, #section_type_value
                                                                              0, #data_type_value
                                                                              0, #rf_vector
                                                                              self.line_count_data)
                                                                              
        self.output_str += self.output_str_data
        self.line_count += self.line_count_data

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionIniGeneralParameters(SectionIni):
    def __init__(self, dark_blue_output):
        SectionIni.__init__(self, dark_blue_output)

        self.output_str_data, self.line_count_data = generate_general_parameters_section(dark_blue_output)

        self.output_str, self.line_count = SectionIni.generate_section_header(self,
                                                                              'General Parameters and Baseband Section',
                                                                              GEN_INI_CONSTANTS['PARAMETERS_SECTION_VP'],
                                                                              3, #section_type_value
                                                                              0, #data_type_value
                                                                              0, #rf_vector
                                                                              self.line_count_data)
        self.output_str += self.output_str_data
        self.line_count += self.line_count_data

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_ch_section_header(channel_group_list, section_type_str, sectors_type_str):
    if len(channel_group_list) == 1:
        description_str = ';;;;;;;;;;;;;;;Channel '
        validation_pattern_ch_val = channel_group_list[0]
    else:
        description_str = ';;;;;;;;;;;;;;;Channels '
        validation_pattern_ch_val = 0xC

    vp_bfdw = BitFieldDW(4, 12, 0x900)
    vp_bfdw.add_field(16, 4, validation_pattern_ch_val)
    vp_bfdw.add_field(20, 12, 0xC00)
    if sectors_type_str == 'Sectors_sens':
        if section_type_str == GENERIC_CONSTANTS['TAG_NAMES']['RX']:
            raise Exception('Error in generating section header for TX sensing. TX sensing sub-section is for TX only.')
        elif section_type_str == GENERIC_CONSTANTS['TAG_NAMES']['TX']:
            section_type_value = 5
            vp_bfdw.add_field(0, 4, 0xC)
            section_type_str += ' Sensing Sectors'
    elif sectors_type_str == 'Sectors_tssi':
        section_type_value = 6 
        vp_bfdw.add_field(0, 4, 0xB) 
        section_type_str += ' Tssi Sectors'
    else:
        if section_type_str == GENERIC_CONSTANTS['TAG_NAMES']['RX']:
            section_type_value = 0
            vp_bfdw.add_field(0, 4, 0xE)
            section_type_str += ' Sectors'
        elif section_type_str == GENERIC_CONSTANTS['TAG_NAMES']['TX']:
            section_type_value = 1
            vp_bfdw.add_field(0, 4, 0xD)
            section_type_str += ' Sectors'
        elif section_type_str == 'RfChannelConfig':
            section_type_value = 4
            vp_bfdw.add_field(0, 4, 0xF)
            
    description_str += str(channel_group_list) + ' ' + section_type_str + ';;;;;;;;;;;;;;;'
    
    return description_str, vp_bfdw, section_type_value

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionIniSectors(SectionIni):
    def __init__(self, sector_section_type_str, measured_channel, channel_group_list, dark_blue_output, rf_base_address, sectors_type_str):
        SectionIni.__init__(self, dark_blue_output)
        self.channel_group_list = channel_group_list
        rf_vector = 0
        self.output_str_data, self.line_count_data, rf_vector, line_count = generate_sector_section(dark_blue_output, measured_channel, sector_section_type_str,sectors_type_str)
        description_str, vp_bfdw, section_type_value = generate_ch_section_header(channel_group_list, sector_section_type_str, sectors_type_str)
        self.output_str, self.line_count = SectionIni.generate_section_header(self,
                                                                              description_str,
                                                                              int(vp_bfdw),
                                                                              section_type_value,
                                                                              1, #data_type_value
                                                                              rf_vector,
                                                                              self.line_count_data,
                                                                              GEN_INI_CONSTANTS['DEFAULT_SECTOR_MASK'],
                                                                              rf_base_address)
                                                                              
        self.output_str += self.output_str_data
        self.line_count += line_count

       
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionRfChannelConfig(SectionIni):
    def __init__(self, channel_group_list, dark_blue_output):
        SectionIni.__init__(self, dark_blue_output)
        self.channel_group_list = channel_group_list

        rf_vector = 0 #TODO: handle rf_vector
        self.output_str_data, self.line_count_data = generate_rf_config_section(dark_blue_output, channel_group_list)

        description_str, vp_bfdw, section_type_value = generate_ch_section_header(channel_group_list, 'RfChannelConfig','')

        self.output_str, self.line_count = SectionIni.generate_section_header(self,
                                                                              description_str,
                                                                              int(vp_bfdw),
                                                                              section_type_value,
                                                                              0, #data_type_value
                                                                              rf_vector,
                                                                              self.line_count_data)
               
        self.output_str += self.output_str_data
        self.line_count += self.line_count_data
        
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
class SectionTssiSectors(SectionIni):
    def __init__(self, channel_group_list, dark_blue_output):
        SectionIni.__init__(self, dark_blue_output)
        self.channel_group_list = channel_group_list
        
        self.output_str_data, self.line_count_data, rf_vector, line_count = generate_tssi_section(dark_blue_output, channel_group_list)

        description_str, vp_bfdw, section_type_value = generate_ch_section_header(channel_group_list, GENERIC_CONSTANTS['TAG_NAMES']['TSSI_TX_SECTOR'],'Sectors_tssi')

        self.output_str, self.line_count = SectionIni.generate_section_header(self,
                                                                              description_str,
                                                                              int(vp_bfdw),
                                                                              section_type_value,
                                                                              1, #data_type_value
                                                                              rf_vector,
                                                                              self.line_count_data,
                                                                              GEN_INI_CONSTANTS['DEFAULT_SECTOR_MASK'],
                                                                              HW_DEFINITIONS[dark_blue_output['GP_Labels'][GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['TX_SECTOR_ADDRESS'] + (32*CUSTOMIZATIONS['TSSI_SECTOR']))
               
        self.output_str += self.output_str_data
        self.line_count += line_count
        

#-------------------------------------------------------------------------------
#  Functions
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def write_ini_line(param1, param2, comment):
    return ("0x%08X = 0x%08X %s\n" % ((param1 & 0xffffffff), (param2 & 0xffffffff), comment))

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def write_ini_header(section_list):

    output_str = ''
    output_str += ("[PRODUCTION]\n")
    output_str += (";;; Board File Format Version\n")
    output_str += write_ini_line(GEN_INI_CONSTANTS['BOARD_FILE_FORMAT_VERSION_VP'], GEN_INI_CONSTANTS['BOARD_FILE_FORMAT_SUPPORTED_VERSION'],GEN_INI_CONSTANTS['BOARD_FILE_FORMAT_TEXT_DESCRIPTION'])

    toc_num_of_lines = GEN_INI_CONSTANTS['NUM_OF_GENERAL_SECTIONS'] + (len(CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']) * CUSTOMIZATIONS['NUM_OF_SECTIONS_PER_CHANNEL'])
    current_byte_offset = toc_num_of_lines * GEN_INI_CONSTANTS['DWORDS_PER_LINE'] * GEN_INI_CONSTANTS['BYTES_PER_DWORD']
    toc_byte_offset = current_byte_offset

    #calculate actual offsets according to measured channels
    for current_section in section_list:
        current_section.set_byte_offset(current_byte_offset)
        current_byte_offset += current_section.get_byte_length()

    size_str = "%.2f" % (float(current_byte_offset)/1024.0)
    output_str += '\n;; Board file total size: ' + str(current_byte_offset) + ' Bytes (' + size_str + ' KB)\n\n'
    output_str += (";;;;;;;;;;;;;;;Production_Ptr_table;;;;;;;;;;;;;;;\n")
    output_str += (";; address = lines\n")
    output_str += (";; Base = " + str(toc_num_of_lines) + " lines * 2 addr/data * 4bytes = " + str(toc_byte_offset) + " bytes\n")

    output_str += '\n;; Common sections\n'
    #Common RF Section
    output_str += str(section_list[0].get_byte_offset()) + '\t=\t' + str(section_list[0].get_line_count()) + ' ;; Common RF Section\n'

    #General Parameters and Baseband Section
    output_str += str(section_list[1].get_byte_offset()) + '\t=\t' + str(section_list[1].get_line_count()) + ' ;; General Parameters and Baseband Section\n'

    #channel sections
    
    channel_desc_dict = {
        1 : '(58.32 GHz)',
        2 : '(60.48 GHz)',
        3 : '(62.64 GHz)',
        4 : '(64.80 GHz)',
        5 : '(66.96 GHz)',
        6 : '(69.12 GHz)',
        9 : ' bonding of channels 1&2',
        10 : 'bonding of channels 2&3',
        11 : 'bonding of channels 3&4',
        12 : 'bonding of channels 4&5',
    }

    for toc_channel in sorted(CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']):
   
        output_str += '\n;; Channel ' + str(toc_channel) + " " + channel_desc_dict[toc_channel] + '\n'
        desc_idx = 0
        for current_section in section_list:
            if current_section.is_channel_in_channel_group_list(toc_channel) == True:
                output_str += str(current_section.get_byte_offset()) + '\t=\t' + str(current_section.get_line_count()) + CUSTOMIZATIONS['PER_CHANNEL_DESC_LIST'][desc_idx] + '\n'
                
                desc_idx += 1

    output_str += '\n\n\n'
    
    return output_str

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def list_to_bit_vector(bit_list):
    bit_vector = 0
    if bit_list is None:
        return bit_vector

    for bit in bit_list:
        bit_vector |= (1 << bit)

    return bit_vector

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_general_parameters_section(dark_blue_output):
    gp_table_dict = dark_blue_output['GP_Labels']
    if dark_blue_output['GP_Sens_Labels'] and CUSTOMIZATIONS['SUPPORT_SENSING'] == True:
        gp_sens_table_dict = dark_blue_output['GP_Sens_Labels']
    else:
        gp_sens_table_dict = []
    output_str = ''
    line_count = 0

    #ID
    output_str_id, line_count_id = generate_gp_id(dark_blue_output)
    line_count += line_count_id
    output_str += output_str_id
    
    #XIFS BIAS
    output_str_xif_bias, line_count_xif_bias = generate_xif_bias(gp_table_dict)
    line_count += line_count_xif_bias
    output_str += output_str_xif_bias
    
    if CUSTOMIZATIONS['SUPPORT_SENSING'] == True:
        #ID sensing 
        output_str_sens_id, line_count_sens_id = generate_gp_sens_id(dark_blue_output)
        line_count += line_count_sens_id
        output_str += output_str_sens_id
        

    #RF sets
    output_str_rf_sets, line_count_rf_sets = generate_gp_rf_sets(dark_blue_output)
    line_count += line_count_rf_sets
    output_str += output_str_rf_sets

    #RF sens sets 
    if dark_blue_output['GP_Sens_Labels'] and CUSTOMIZATIONS['SUPPORT_SENSING'] == True:
        output_str_rf_sens_sets, line_count_rf_sens_sets= generate_gp_rf_sens_sets(dark_blue_output)
        line_count += line_count_rf_sens_sets
        output_str += output_str_rf_sens_sets

    #BRP struct
    output_str_brp_struct, line_count_brp_struct = generate_gp_brp_struct(gp_table_dict, gp_sens_table_dict)
    line_count += line_count_brp_struct
    output_str += output_str_brp_struct
    
    if True == CUSTOMIZATIONS['ADD_TPC_LIMIT']:
        #TPC max allowed power in dbm mul10
        output_str_tx_power, line_count_tx_power  = generate_gp_tpc(gp_table_dict)
        line_count += line_count_tx_power
        output_str += output_str_tx_power

    if True == CUSTOMIZATIONS['ADD_LO_POWER_TARGET_LIMIT']:
        # LO power target
        output_str_lo_power, line_count_lo_power = generate_gp_lo_power(gp_table_dict)
        line_count += line_count_lo_power
        output_str += output_str_lo_power
   
    #Add TSSI information
    if CUSTOMIZATIONS['TSSI_SECTOR'] != None:
        output_str_tssi, line_count_tssi = generate_gp_tssi(dark_blue_output)
        line_count += line_count_tssi
        output_str += output_str_tssi

    #TX gain table
    output_str_tx_gain_tbl, line_count_tx_gain_tbl  = generate_gp_lineups(gp_table_dict, dark_blue_output['Classification'])
    line_count += line_count_tx_gain_tbl
    output_str += output_str_tx_gain_tbl

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_lineup_table(table_str, lineups_type, lineups_group):
    output_str = ''
    line_count = 0

    if table_str == 'RF_TX_TABLE_BRD_LABEL_ADDR':
        output_str += '\n;;Tx Gain Table, '
    elif table_str == 'RF_RX_G0_TABLE_BRD_LABEL_ADDR':
        output_str += '\n;;G0 Gain Table, '
    elif table_str == 'RF_RX_G1_TABLE_BRD_LABEL_ADDR':
        output_str += '\n;;G1 Gain Table, '

    output_str += 'lineups_type = ' + lineups_type + '\n'

    if table_str == 'RF_TX_TABLE_BRD_LABEL_ADDR':
        tbl_root = lineups_group.find('TX_Classification_Table')
    elif table_str == 'RF_RX_G0_TABLE_BRD_LABEL_ADDR':
        tbl_root = lineups_group.find('G0_Classification_Table')
    elif table_str == 'RF_RX_G1_TABLE_BRD_LABEL_ADDR':
        tbl_root = lineups_group.find('G1_Classification_Table')

    # typedef PREPACK struct
    # {
    #    //DW1 [rows_num][t_slope][spcaing]
    #    U08 spacing_mul10;
    #    U08 tslope_mul10;
    #    U16 table_rows_num;
    #    //DW2  [typical_lineup][offset]
    #    S16 offset;
    #    U16 typical_lineup;
    #    //DW3  [offset_bpsk][offset_qpsk]
    #    S16 offset_qpsk;
    #    S16 offset_bpsk;
    # }POSTPACK meta_data_s;
    lineup_spacing_mul10 = int(float(tbl_root.find('Spacing').text) * 10)
    if table_str == 'RF_TX_TABLE_BRD_LABEL_ADDR':
        lineup_tslope_mul10 = int(float(tbl_root.find('Sliding_Window_TSlope').text) * 10)
    elif table_str == 'RF_RX_G0_TABLE_BRD_LABEL_ADDR':
        lineup_tslope_mul10 = 0 #reserved
    elif table_str == 'RF_RX_G1_TABLE_BRD_LABEL_ADDR':
        lineup_tslope_mul10 = 0 #reserved

    lineup_table_rows_num = 0
    lineups_output_str = ''
    for lineup in tbl_root.findall('lineup'):
        lineups_output_str += lineup.text + '\n'
        lineup_table_rows_num += 1
    bfdw = BitFieldDW(0, 8, lineup_spacing_mul10)
    bfdw.add_field(8, 8, lineup_tslope_mul10)
    bfdw.add_field(16, 16, lineup_table_rows_num)
    lineup_metadata_dw1 = int(bfdw)

    output_str += write_ini_line(GEN_INI_CONSTANTS[table_str], lineup_metadata_dw1, '')
    line_count += 1

    if table_str == 'RF_TX_TABLE_BRD_LABEL_ADDR':
        dw2_bits_15_0 = int(tbl_root.find('G0_TX_Offset_QAM').text)
    elif table_str == 'RF_RX_G0_TABLE_BRD_LABEL_ADDR':
        dw2_bits_15_0 = 0 #reserved
    elif table_str == 'RF_RX_G1_TABLE_BRD_LABEL_ADDR':
        dw2_bits_15_0 = int(tbl_root.find('G0_G1_Offset').text)
    Typical_Lineup = int(tbl_root.find('Typical_Lineup').text)
    bfdw = BitFieldDW(0, 16, dw2_bits_15_0)
    bfdw.add_field(16, 16, Typical_Lineup)
    lineup_metadata_dw2 = int(bfdw)

    if table_str == 'RF_TX_TABLE_BRD_LABEL_ADDR':
        lineup_G0_TX_Offset_QPSK = int(tbl_root.find('G0_TX_Offset_QPSK').text)
        lineup_G0_TX_Offset_BPSK = int(tbl_root.find('G0_TX_Offset_BPSK').text)
    elif table_str == 'RF_RX_G0_TABLE_BRD_LABEL_ADDR':
        lineup_G0_TX_Offset_QPSK = 0 #reserved
        lineup_G0_TX_Offset_BPSK = 0 #reserved
    elif table_str == 'RF_RX_G1_TABLE_BRD_LABEL_ADDR':
        lineup_G0_TX_Offset_QPSK = 0  # reserved
        lineup_G0_TX_Offset_BPSK = 0  # reserved

    bfdw = BitFieldDW(0, 16, lineup_G0_TX_Offset_QPSK)
    bfdw.add_field(16, 16, lineup_G0_TX_Offset_BPSK)
    lineup_metadata_dw3 = int(bfdw)

    output_str += write_ini_line(lineup_metadata_dw2, lineup_metadata_dw3, '')
    line_count += 1

    output_str += lineups_output_str
    line_count += lineup_table_rows_num

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_lineups(gp_table_dict, classification_xml_root):
    output_str = ''
    line_count = 0

    lineups_group, lineups_type = get_classification_group_root(classification_xml_root, gp_table_dict)
    
    #TX gain table
    lineups_output_str, lineups_line_count = generate_lineup_table('RF_TX_TABLE_BRD_LABEL_ADDR', lineups_type, lineups_group)
    output_str += lineups_output_str
    line_count +=lineups_line_count

    # G0_Classification_Table
    lineups_output_str, lineups_line_count = generate_lineup_table('RF_RX_G0_TABLE_BRD_LABEL_ADDR', lineups_type, lineups_group)
    output_str += lineups_output_str
    line_count +=lineups_line_count

    # G1_Classification_Table
    lineups_output_str, lineups_line_count = generate_lineup_table('RF_RX_G1_TABLE_BRD_LABEL_ADDR', lineups_type, lineups_group)
    output_str += lineups_output_str
    line_count +=lineups_line_count

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_classification_group_root(classification_xml_root, gp_table_dict):
    lineups_type = gp_table_dict['Lineups']
    for lineups_group in classification_xml_root.findall('Group'):
        if lineups_group.attrib['Name'] == lineups_type:
            break
    return lineups_group, lineups_type
    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_tssi(dark_blue_output):
    
    line_count = 0
    output_str = '\n;; TSSI Information per channel\n'
    
    gp_table_dict = dark_blue_output['GP_Labels']
    
    bfdw = BitFieldDW(0,7,0) #reserved
    bfdw.add_field(8,15,CUSTOMIZATIONS['TSSI_SECTOR'])
    bfdw.add_field(16,23,gp_table_dict[GENERIC_CONSTANTS['INTERNAL']['TSSI_STRUCT']]['rx_sector']) 
    bfdw.add_field(24,31,0)#reserved
    
    output_str += write_ini_line(GEN_INI_CONSTANTS['TSSI_INFORMATION_LABEL_ADDR'],
                                 int(bfdw), 
                                 ';; (label , [0:7]reserved;[8:15]TX sector_num; [16:23]RX sector_num; [24:30]reserved; [31]create sector)')
    line_count += 1

    for ch in GENERIC_CONSTANTS['SUPPORTED_CHANNELS']:
        rf_3_2_1_0 = 0
        rf_7_6_5_4 = 0
        
        rfList = gp_table_dict['TSSICalibStructure'][ch].keys()
        
        if (len(rfList) == 1) and (-1 == rfList[0]):
            genericRf = True
        else:
            genericRf = False
            
        bfdw = BitFieldDW(0,31,0)
        for rfIndex in range (0, 4):
            if True == genericRf:
                bfdw.add_field((rfIndex*8),(rfIndex*8)+7,gp_table_dict['TSSICalibStructure'][ch][-1]['chain_number'])
            else:
                if rfIndex in rfList:
                    bfdw.add_field((rfIndex*8),(rfIndex*8)+7,gp_table_dict['TSSICalibStructure'][ch][rfIndex]['chain_number'])
                else:
                    bfdw.add_field((rfIndex*8),(rfIndex*8)+7,0)
        rf_3_2_1_0 = int(bfdw)
        bfdw = BitFieldDW(0,31,0)
        for rfIndex in range (4, 8):
            if True == genericRf:
                bfdw.add_field(((rfIndex-4)*8),((rfIndex-4)*8)+7,gp_table_dict['TSSICalibStructure'][ch][-1]['chain_number'])
            else:
                if rfIndex in rfList:
                    bfdw.add_field(((rfIndex-4)*8),((rfIndex-4)*8)+7,gp_table_dict['TSSICalibStructure'][ch][rfIndex]['chain_number'])
                else:
                    bfdw.add_field(((rfIndex-4)*8),((rfIndex-4)*8)+7,0)
        rf_7_6_5_4 = int(bfdw)
            
        output_str += write_ini_line(rf_3_2_1_0, 
                                     rf_7_6_5_4, 
                                    ';; <CH' + str(ch) + '> : <ant_rf_3,....,ant_rf_0> = <ant_rf_7,....,ant_rf_4>')
        line_count += 1
        
    return output_str, line_count



#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_tpc(gp_table_dict):
    line_count = 0
    output_str = '\n;;TPC max allowed power in dbm mul10\n'
	#Assuming same limitation for all RFCs, using RFC 0 
    tpc_max_allowed_power_float = gp_table_dict['ScalarPerRFCStructure']['TPC'][0]
    tpc_max_allowed_power_mul10_int = int(tpc_max_allowed_power_float*10)
    output_str += write_ini_line(GEN_INI_CONSTANTS['TPC_MAX_ALLOWED_POWER_BRD_LABEL_ADDR'],
                                 tpc_max_allowed_power_mul10_int, '')
    line_count += 1

    return output_str, line_count


# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
def generate_gp_lo_power(gp_table_dict):
    line_count = 0

    lo_power_dict = []
    lo_power_dwords_list = [0,0]
    lo_power_index = 0

    output_str = '\n;;LO Power Target Limit\n'
    output_str += write_ini_line(GEN_INI_CONSTANTS['LO_BB_POWER_LIMIT_BRD_LABEL_ADDR'], 0, '')
    line_count += 1

    lo_target_power_dict = gp_table_dict['ScalarPerRFCStructure']['LO_TARGET_POWER']

    for rf_couple in range(0,2,1):
        bfdw = BitFieldDW(0, 1, 0)
        for index in range(0,4,1):
            bfdw.add_field(index * 8, 8, int(lo_target_power_dict[rf_couple*4 + index]))
        lo_power_dwords_list[rf_couple] = int(bfdw)

    output_str += write_ini_line(lo_power_dwords_list[0], lo_power_dwords_list[1], '')
    line_count += 1

    return output_str, line_count

# -------------------------------------------------------------------------------
# -------------------------------------------------------------------------------
def valid_to_invalid_anntennas(valid_ant_list):
    invalid_ant_list = []
    for cur_ant in range(0,32,1):
        if cur_ant not in valid_ant_list:
            invalid_ant_list.append(cur_ant)

    return invalid_ant_list
    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_brp_struct(gp_table_dict, gp_sens_table_dict):
    output_str = '\n;;Oscillating and not connected RX antennas\n'
    brp_structure_dict = gp_table_dict['BRPStructure']
    if gp_sens_table_dict != []:
        brp_sens_structure_dict = gp_sens_table_dict['BRPStructure']

    CONSTANTS=init_CONSTANTS();
    valid_antennas_per_rf_dw_list = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(0, 8, 1):
        if gp_sens_table_dict == []:
            valid_antennas_per_rf_dw_list[i] = list_to_bit_vector(valid_to_invalid_anntennas(brp_structure_dict[i]))
        else:
            if brp_structure_dict[i] != CONSTANTS['DEFAULT_UNUSED_BRP_ANTENNAS']:
                valid_antennas_per_rf_dw_list[i] = list_to_bit_vector(valid_to_invalid_anntennas(brp_structure_dict[i]))
            else:
                valid_antennas_per_rf_dw_list[i] = list_to_bit_vector(valid_to_invalid_anntennas(brp_sens_structure_dict[i]))

    output_str += write_ini_line(GEN_INI_CONSTANTS['VALID_ANTENNAS_BRD_LABEL_ADDR'], valid_antennas_per_rf_dw_list[0],
                                 ';;<Address (0x8fffff)>  =  <Valid Antennas for RFC0>')
    line_count = 1
    for idx in range(1,6,2):
        comment_string = ';;<Valid Antennas for RFC' + str(idx) + '>  =  <Valid Antennas for RFC' + str(idx+1) + '>'
        output_str += write_ini_line(valid_antennas_per_rf_dw_list[idx], valid_antennas_per_rf_dw_list[idx+1], comment_string)
        line_count += 1

    bfdw = BitFieldDW(0,8,gp_table_dict['BRPStructure']['MAX_BRP_PER_TILE'])
    bfdw.add_field(8,8,gp_table_dict['BRPStructure']['MAX_BRP_OVERALL_SHORT_RANGE']) 
    bfdw.add_field(16,16,gp_table_dict['BRPStructure']['MAX_BRP_OVERALL'])
    max_brp_antennas_dw = int(bfdw)
    output_str += write_ini_line(valid_antennas_per_rf_dw_list[7], max_brp_antennas_dw, ';;<Valid Antennas for RFC7>  =  <Max BRP Antennas>')
    line_count += 1

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_rf_sens_sets(dark_blue_output):
    output_str = '\n;; Sensing RF sets definitions\n'
    output_str += ';; Section Header = number of SLS sectors\n'

    gp_sens_table_dict = dark_blue_output['GP_Sens_Labels']

    num_of_tx_ssw_sectors = gp_sens_table_dict['RFsets_dict'][0]['TX_sectors_num']
    output_str += write_ini_line(GEN_INI_CONSTANTS['RF_SENS_SETS_INFO_LABEL'], num_of_tx_ssw_sectors, '')
    line_count = 1
    output_str += ';; Sensing RF pairs (Bit Mask): Address field: (31:24)Pair 1,TX-RF ;(23:16)Pair 1,RX-RF ;(15:8)Pair 0,TX-RF ;(7:0)Pair 0,RX-RF ;\n'
    output_str += ';; Sensing RF pairs (Bit Mask): Data Filed  : (31:24)Pair 3,TX-RF ;(23:16)Pair 3,RX-RF ;(15:8)Pair 2,TX-RF ;(7:0)Pair 2,RX-RF ;\n'

  
    rf_set_dwords_list = generate_rf_sens_set_dwords(gp_sens_table_dict)
    output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],'')
    line_count += 1

    output_str += ';; first SLS TX-sector per RF\n'
    first_sector_vec = generate_rf_sens_first_ssw_sector_indx(dark_blue_output)
    output_str += ';;<RF0> = <RF1> \n'
    output_str += write_ini_line(first_sector_vec[0], first_sector_vec[1],'')
    line_count += 1
    output_str += ';;<RF2> = <RF3> \n'
    output_str += write_ini_line(first_sector_vec[2], first_sector_vec[3],'')
    line_count += 1
    output_str += ';;<RF4> = <RF5> \n'
    output_str += write_ini_line(first_sector_vec[4], first_sector_vec[5],'')
    line_count += 1
    output_str += ';;<RF6> = <RF7> \n'
    output_str += write_ini_line(first_sector_vec[6], first_sector_vec[7],'')
    line_count += 1

    output_str += ';; Sensing sample offset\n'
    output_str += write_ini_line(gp_sens_table_dict['sensing_sample_offset'], 0,'')
    line_count += 1

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_rf_sets(dark_blue_output):
    output_str = '\n; RF sets definitions\n'
    
    gp_table_dict = dark_blue_output['GP_Labels']
    
    boardfile_type, rf_sets_num = get_boardfile_type(gp_table_dict)
    
    if 'V2' == CUSTOMIZATIONS['RF_SETS_MAPPING']:
        label = GEN_INI_CONSTANTS['RF_SETS_INFO_LABEL_V2']
    else:
        label = GEN_INI_CONSTANTS['RF_SETS_INFO_LABEL_V1']

    output_str += write_ini_line(label, label, '')
    line_count = 1
    
    assert(len(gp_table_dict['RFsets_dict'].keys()) <= GEN_INI_CONSTANTS['MAX_NUM_RF_SETS'])
    
    if False == CUSTOMIZATIONS['ALLOW_EMPTY_RF_SET']:
        for rf_set in gp_table_dict['RFsets_dict'].keys():
            assert(gp_table_dict['RFsets_dict'][rf_set]['RFCs_RX'] != [])
            assert(gp_table_dict['RFsets_dict'][rf_set]['RFCs_TX'] != [])
    
    if 'V2' == CUSTOMIZATIONS['RF_SETS_MAPPING']:
        rf_set_dwords_listRx = generate_rf_set_dwords(gp_table_dict, 'RFCs_RX') 
        rf_sets_rx_mask = get_active_rf_set_mask(gp_table_dict, 'RFCs_RX')
        rf_set_dwords_listTx = generate_rf_set_dwords(gp_table_dict, 'RFCs_TX') 
        rf_sets_tx_mask = get_active_rf_set_mask(gp_table_dict, 'RFCs_TX')
        
        output_str += write_ini_line(rf_set_dwords_listRx[0], rf_set_dwords_listTx[0],
                                     ';; <available RX RFC at RF Sets 0..3>  =  <available TX RFC at RF Sets 0..3>')
        line_count += 1
        
        channelsList = list()
        for channel in dark_blue_output['Sectors']:
            channelsList.append(channel)
            
        assert(len(channelsList) > 0)
        
        nearest_measured_ch = generate_sector_channel_dict(channelsList)
        
        #specify max number of sectors per channel
        for ch in GENERIC_CONSTANTS['SUPPORTED_CHANNELS']:
            
            refCh = nearest_measured_ch[ch]
            
            max_num_tx_sectors = 0
            max_num_rx_sectors = 0
            
            for rf in sorted(dark_blue_output['Sectors'][refCh]['rx']):
                sector_count_per_channel = max(dark_blue_output['Sectors'][refCh]['rx'][rf].keys()) + 1
                max_num_rx_sectors = max(max_num_rx_sectors, sector_count_per_channel)
            
            for rf in sorted(dark_blue_output['Sectors'][refCh]['tx']):
                sector_count_per_channel = max(dark_blue_output['Sectors'][refCh]['tx'][rf].keys()) + 1
                max_num_tx_sectors = max(max_num_tx_sectors, sector_count_per_channel)
            
            rf_set_dwords_listRx = generate_rf_set_dwords_from_val(gp_table_dict, max_num_rx_sectors)
            rf_set_dwords_listTx = generate_rf_set_dwords_from_val(gp_table_dict, max_num_tx_sectors)
            
            output_str += write_ini_line((rf_set_dwords_listRx[0] & rf_sets_rx_mask), (rf_set_dwords_listTx[0] & rf_sets_tx_mask), 
                                        ';; <CH' + str(ch) + '> : <RX Sectors per RF Set 0..3> = <TX Sectors per RF Set 0..3>')
            line_count += 1
    else:
        rf_set_dwords_list = generate_rf_set_dwords(gp_table_dict, 'RFCs') 
        output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],
                                     ';;available RFC at <RF Sets 0..3>  =  <RF Sets 4..7>')
        line_count += 1
    
        if boardfile_type != 'single':

            #massive and diversity or massive-diversity
            max_num_tx_sectors = 0
            max_num_rx_sectors = 0
            
            for channel in sorted(dark_blue_output['Sectors']):
                for rf in sorted(dark_blue_output['Sectors'][channel]['tx']):
                    sector_count_per_channel = max(dark_blue_output['Sectors'][channel]['tx'][rf].keys()) + 1
                    max_num_tx_sectors = max(max_num_tx_sectors, sector_count_per_channel)

            for channel in sorted(dark_blue_output['Sectors']):
                for rf in sorted(dark_blue_output['Sectors'][channel]['rx']):
                    sector_count_per_channel = max(dark_blue_output['Sectors'][channel]['rx'][rf].keys()) + 1
                    max_num_rx_sectors = max(max_num_rx_sectors, sector_count_per_channel)
                    
            rf_set_dwords_list = generate_rf_set_dwords_from_val(gp_table_dict, max_num_tx_sectors)
            output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],
                                         ';;<TX Sectors per RF Set 0..3> = <TX Sectors per RF Set 4..7>')
                                         
            line_count += 1
            rf_set_dwords_list = generate_rf_set_dwords_from_val(gp_table_dict, max_num_rx_sectors)
            output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],
                                         ';;<RX Sectors per RF Set 0..3> = <RX Sectors per RF Set 4..7>')
            line_count += 1
            
        else:
            #single
            rf_set_dwords_list = generate_rf_set_dwords(gp_table_dict, 'TX_sectors_num')
            output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],
                                         ';;<TX Sectors per RF Set 0..3> = <TX Sectors per RF Set 4..7>')
                                         
            line_count += 1
            rf_set_dwords_list = generate_rf_set_dwords(gp_table_dict, 'RX_sectors_num')
            output_str += write_ini_line(rf_set_dwords_list[0], rf_set_dwords_list[1],
                                         ';;<RX Sectors per RF Set 0..3> = <RX Sectors per RF Set 4..7>')
            line_count += 1
        
    return output_str, line_count
    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_xif_bias(gp_table_dict): 

    xif_bias_dic = gp_table_dict[GENERIC_CONSTANTS['INTERNAL']['SCALAR_PER_RFC_STRUCT']]['DC_SOURCE']
    
    mask = 0
    for xif in xif_bias_dic.keys():
        mask |= (int(xif_bias_dic[xif]) << xif)
    
    if mask == 0xFF:
        output_str = ""
        line_count = 0
    else:
        output_str = '\n;; XIF bias definitions\n'
        output_str += write_ini_line(GEN_INI_CONSTANTS['XIFS_BIAS_DEFINITION_LABEL_ADDR'], mask,
                                             ';;<XIF bias definition label> = <XIF bias bitmask per RF>')
        line_count = 1
    
    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_id(dark_blue_output): 

    gp_table_dict = dark_blue_output['GP_Labels']

    sectors_counter_id = int(gp_table_dict['ID']['identifier'],16)
    maker_id = gp_table_dict['ID']['maker']

    classification_group_root, classification_type_str = get_classification_group_root(dark_blue_output['Classification'], gp_table_dict)
    classification_revision = int(classification_group_root.attrib['Revision'])
    classification_group = ord(classification_type_str) - ord('A')
   
    silicon_id = HW_DEFINITIONS[gp_table_dict[GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['SILICON_ID']
    
    id_val = BitFieldDW(0,16,sectors_counter_id)
    id_val.add_field(16,6,classification_revision)
    id_val.add_field(22,3,classification_group)
    id_val.add_field(25,3,maker_id)
    id_val.add_field(28,4,silicon_id)

    output_str = ';; ID Register\n'
    output_str += write_ini_line(GEN_INI_CONSTANTS['ID_REG_ADDRESS'], int(id_val), '')
    line_cnt = 1

    #TODO: Remove! added for simplicity of diff...
    output_str += '\n;;platform_flags.rf_kill_hw_enable = 0\n'
    output_str += write_ini_line(0x880A94, 0, '')
    line_cnt += 1
    
    if dark_blue_output['FwVersion'] == "":
        fw_version = CUSTOMIZATIONS['FW_VERSION']
    else:
        fw_version = dark_blue_output['FwVersion']
    output_str += '\n;; FW version used for board file generation: ' + fw_version + '\n'
    output_str += write_ini_line(GEN_INI_CONSTANTS['FW_VERSION_LABEL_ADDR'], int(fw_version), '')
    line_cnt += 1

    return output_str, line_cnt

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_gp_sens_id(dark_blue_output): 

    if dark_blue_output['GP_Sens_Labels']:
        gp_table_dict = dark_blue_output['GP_Labels']
        gp_sens_table_dict = dark_blue_output['GP_Sens_Labels']

        sens_sectors_id = int(gp_sens_table_dict['ID_sens_sectors']['identifier'],16)
        if gp_sens_table_dict['ID_sens_params']!= {}:
            sens_params_id = int(gp_sens_table_dict['ID_sens_params']['identifier'],16)
        else:
            raise Exception('Sens_params.xml has an empty ID!')
        maker_id = gp_sens_table_dict['ID_sens_sectors']['maker']
        silicon_id = HW_DEFINITIONS[gp_sens_table_dict[GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['SILICON_ID']

        id_val = BitFieldDW(0,12,sens_sectors_id)
        id_val.add_field(12,12,sens_params_id)
        id_val.add_field(24,1,0)
        id_val.add_field(25,3,maker_id)
        id_val.add_field(28,4,silicon_id)
    else:
        id_val = BitFieldDW(0,32,0)

    output_str = '\n;; ID Register for sensing\n'
    output_str += write_ini_line(GEN_INI_CONSTANTS['ID_SENS_REG_ADDRESS'], int(id_val), '')
    line_cnt = 1

    return output_str, line_cnt

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def get_boardfile_type(gp_table_dict):
    rf_sets_dict = gp_table_dict['RFsets_dict']
    rf_sets_num = len(rf_sets_dict)
    boardfile_type_str = ''
    if rf_sets_num == 1:
        if len(rf_sets_dict[0]['RFCs']) == 1:
            boardfile_type_str = 'single'
        else:
            boardfile_type_str = 'massive'
    else:
        boardfile_type_str = 'diversity'
        for i in range (0, rf_sets_num):
            if len(rf_sets_dict[i]['RFCs']) > 1:
                boardfile_type_str = 'diversity_massive' #if any of the RF sets has more than 1 RF it is diversity massive

    return boardfile_type_str, rf_sets_num


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_rf_set_dwords(gp_table_dict, rf_set_parameter):
    rf_sets_dict = gp_table_dict['RFsets_dict']
    rf_set_idx = 0
    rf_set_dwords_list = [0, 0]
    for rf_set in rf_sets_dict.keys():
        rf_set_list = rf_sets_dict[rf_set][rf_set_parameter]
        if (rf_set_list != None) and (rf_set_list != []) and (rf_set_list[0] != -1):
            rf_set_byte = list_to_bit_vector(rf_set_list)
            rf_set_dwords_list[0] |= (rf_set_byte << (rf_set * 8))
            rf_set_idx += 1
            assert(rf_set_idx <= GEN_INI_CONSTANTS['MAX_NUM_RF_SETS'])
    return rf_set_dwords_list
    
    
    #-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_active_rf_set_mask(gp_table_dict, rf_set_parameter):
    rf_sets_dict = gp_table_dict['RFsets_dict']
    rf_set_idx = 0
    mask = 0
    for rf_set in rf_sets_dict.keys():
        rf_set_list = rf_sets_dict[rf_set][rf_set_parameter]
        if (rf_set_list != []) and (rf_set_list != None):
            mask |= 0xFF << (rf_set * 8)
        assert(rf_set_idx <= GEN_INI_CONSTANTS['MAX_NUM_RF_SETS'])
        
    return mask

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_rf_sens_set_dwords(gp_table_dict):
# for Sensing there are only 4 sets. 
# each test contains a byte for enable TX RFCs and byte to enable RX RFCs
    rf_sets_dict = gp_table_dict['RFsets_dict']
    rf_set_idx = 0
    rf_set_dwords_list = [0, 0]
    for rf_set in rf_sets_dict.keys():
        rf_rx_set_list = rf_sets_dict[rf_set]['RX_RFCs']
        rf_tx_set_list = rf_sets_dict[rf_set]['TX_RFCs']
        if ((rf_rx_set_list != None) and (rf_rx_set_list[0] != -1) and (rf_tx_set_list != None) and (rf_tx_set_list[0] != -1)):
            rf_rx_set_byte = list_to_bit_vector(rf_rx_set_list)
            rf_tx_set_byte = list_to_bit_vector(rf_tx_set_list)
            if rf_set_idx < 2:
                list_index = 0
            else:
                list_index = 1
                rf_set_idx = 0
            rf_set_dwords_list[list_index] |= ((rf_tx_set_byte << (rf_set_idx * 16 + 8)) | (rf_rx_set_byte << (rf_set_idx * 16)))
            rf_set_idx += 1
    return rf_set_dwords_list

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_rf_sens_first_ssw_sector_indx(dark_blue_output):

    gp_sens_table_dict = dark_blue_output['GP_Sens_Labels']

    gp_table_dict = dark_blue_output['GP_Labels']

    first_sector_vec = [0,0,0,0,0,0,0,0]
    first_sector_val = get_rf_sens_first_ssw_sector_indx(dark_blue_output)


    for curr_rf_set in gp_sens_table_dict['RFsets_dict']:
        curr_rf_list = gp_sens_table_dict['RFsets_dict'][curr_rf_set].get('TX_RFCs')
        for curr_rf in curr_rf_list:
            first_sector_vec[curr_rf] = first_sector_val

    return first_sector_vec
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_rf_sens_first_ssw_sector_indx(dark_blue_output):
    max_sector_ID = 0
    sectors_measured_to_full_channel_dict = get_sectors_measured_to_full_channel_list(dark_blue_output, 'SectorChannelGroups')
    for channel in sorted(sectors_measured_to_full_channel_dict.keys()):
        sector_ID = 0
        for rf in sorted(dark_blue_output['Sectors'][channel]['tx']):
            sector_ID += len(dark_blue_output['Sectors'][channel]['tx'][rf])
        if sector_ID > max_sector_ID:
            max_sector_ID = sector_ID

    GENERIC_CONSTANTS['ILLEGAL_SECTORS'].sort()
    for x in GENERIC_CONSTANTS['ILLEGAL_SECTORS']: # if an illegal sector is in the range of networking sectors, make sure count it in
        if x <= max_sector_ID:
            max_sector_ID += 1

    for x in GENERIC_CONSTANTS['ILLEGAL_SECTORS']: # Make sure TX sensing sectors are not using illegal sectors 38 and 68
        if x in range(max_sector_ID, max_sector_ID + 4): 
            first_sector_val = x + 1

    return max_sector_ID
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_rf_set_dwords_from_val(gp_table_dict, val):
    rf_sets_dict = gp_table_dict['RFsets_dict']
    
    rf_set_idx = 0
    rf_set_dwords_list = [0, 0]
    for rf_set in rf_sets_dict.keys():       
        rf_set_dwords_list[0] |= val << (rf_set_idx * 8)
        rf_set_idx += 1
        assert(rf_set_idx <= GEN_INI_CONSTANTS['MAX_NUM_RF_SETS'])
    return rf_set_dwords_list
    
    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_tssi_section(dark_blue_output, channel_group_list):
    output_str = ''
    line_count = 0
    rf_vec = 0
    rf_list = []
    rf_count = 0
    
    tssiCalibStructure = dark_blue_output['GP_Labels']['TSSICalibStructure']
   
    if [] != channel_group_list:
        channel = channel_group_list[0] #pick one of the channel we search for
        for rf in sorted(tssiCalibStructure[channel]):
            if True == isinstance(rf, int):
                if rf == -1:
                    output_str += '\n;;data only - Single RF' + '\n'
                    rf_count = 1
                else:
                    if tssiCalibStructure[channel][rf]['tx_sector'] != 0: 
                        #add rf information if exists
                        rf_list.append(rf)
                        rf_count += 1
                        output_str += '\n;;data only - rf' + str(rf) + '\n'
                        
                output_str += ';;sector ' + str(CUSTOMIZATIONS['TSSI_SECTOR']) + '\n' + str(tssiCalibStructure[channel][rf]['tx_sector'])
                line_count += GEN_INI_CONSTANTS['LINES_PER_SECTOR']
        
        rf_vec = list_to_bit_vector(rf_list)
        
    return output_str, (line_count/rf_count), rf_vec, line_count



#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_rf_config_section(dark_blue_output, channel_group_list):
    output_str = ''
    line_count = 0

    common_tables_group, group_type = get_classification_group_root(dark_blue_output['Classification'], dark_blue_output['GP_Labels'])
   
    all_tables = None
    
    if [] == channel_group_list:
        all_tables = common_tables_group.find('Common/type_tables')
    else:
        channel = channel_group_list[0] #pick one of the channel we search for
        all_channels_tables = common_tables_group.findall('Channel')
        for obj in all_channels_tables:
            channels_str = obj.attrib['Num']
            channels_list = channels_str.split()
            if str(channel) in channels_list:
                #found the relevant table
                all_tables = obj.find('type_tables')
                break
                
    assert(all_tables != None)
    
    #write the data
    for table in all_tables:
        output_str += (";;%s, table type = %s\n" % (table.tag, group_type))
        for line in table:
            output_str += ("%s\n" % (line.text))
            line_count += 1
        output_str += ("\n")

    return output_str, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_sectors_measured_to_full_channel_list(dark_blue_output, channel_group_type):
    unique_channel_list = []
    measured_to_full_channel_dict = {}

    for ch in dark_blue_output[channel_group_type]:
        if ch in CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']:
            found = 0
            for unique_ch in unique_channel_list:
               if dark_blue_output[channel_group_type][ch] == unique_ch:
                    found = 1
            if found == 0:
                unique_channel_list.append(dark_blue_output[channel_group_type][ch])
                measured_to_full_channel_dict[dark_blue_output[channel_group_type][ch]] = [ch]
            else:
                measured_to_full_channel_dict[dark_blue_output[channel_group_type][ch]].append(ch)

    return measured_to_full_channel_dict

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_rf_configure_channel_group_list(dark_blue_output):
    channel_groups_list = []
    channel_groups_to_channels_dict = {}

    #group numbering is according to order in XML, starting from 0

    classification_type = dark_blue_output['GP_Labels']['Lineups']
    
    if classification_type not in dark_blue_output['RFConfigChannelGroups'].keys():
        print classification_type + " does not exist in classification file"
        raise ValueError("Missing lineup information")

    for ch in dark_blue_output['RFConfigChannelGroups'][classification_type]:
        if ch in CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']:
            found = 0
            for unique_ch in channel_groups_list:
               if dark_blue_output['RFConfigChannelGroups'][classification_type][ch] == unique_ch:
                    found = 1
            if found == 0:
                channel_groups_list.append(dark_blue_output['RFConfigChannelGroups'][classification_type][ch])
                channel_groups_to_channels_dict[dark_blue_output['RFConfigChannelGroups'][classification_type][ch]] = [ch]
            else:
                channel_groups_to_channels_dict[dark_blue_output['RFConfigChannelGroups'][classification_type][ch]].append(ch)

    return channel_groups_to_channels_dict
    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def identifyProblematicSector(absolote_sector_id, sector_count_per_rf):
    rf_index = 0
    relative_sector_id = absolote_sector_id
    
    while relative_sector_id > sector_count_per_rf:
        rf_index += 1
        relative_sector_id -= sector_count_per_rf
        
    return rf_index, relative_sector_id

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_sector_section(dark_blue_output, channel, section_type_str, sectors_type_str):
    output_str = ''
    line_count = 0
    rf_vec = 0
    rf_list = []
    rf_count = 0
    
    boardfile_type, rf_sets_num = get_boardfile_type(dark_blue_output['GP_Labels'])
    
    max_sector_id = 0
    
    #count sectors for all channels
    if dark_blue_output[sectors_type_str]:
        for rf in sorted(dark_blue_output[sectors_type_str][channel][section_type_str]):
            max_sector_id = max(max_sector_id, max(dark_blue_output[sectors_type_str][channel][section_type_str][rf])) 
            
    if (CUSTOMIZATIONS['TX_SECTORS_PADDING'] != None) and (section_type_str == 'tx'):
        max_sector_id = CUSTOMIZATIONS['TX_SECTORS_PADDING'] #override max sector id in order to generate zero padding up to this sector

    #output sections
    if dark_blue_output[sectors_type_str]:
        for rf in sorted(dark_blue_output[sectors_type_str][channel][section_type_str]):
            
            if rf == -1:
                output_str += '\n;;data only - Single RF' + '\n'
                rf_count = 1
            else:
                rf_list.append(rf)
                rf_count += 1
                output_str += '\n;;data only - rf' + str(rf) + '\n'
            for sector_num in range (0, max_sector_id + 1):
                if sector_num not in dark_blue_output[sectors_type_str][channel][section_type_str][rf].keys():
                    #need to add fake sector 
                    output_str += ';;sector ' + str(sector_num) + '\n0x00000000=0x00000000\n0x00000000=0x00000000\n0x00000000=0x00000000\n0x00000000=0x00000000\n'
                    line_count += GEN_INI_CONSTANTS['LINES_PER_SECTOR']
                else:
                    output_str += ';;sector ' + str(sector_num) + '\n' + str(dark_blue_output[sectors_type_str][channel][section_type_str][rf][sector_num])
                    line_count += GEN_INI_CONSTANTS['LINES_PER_SECTOR']

    rf_vec = list_to_bit_vector(rf_list)
    
    if 0 == rf_count:
        # Make sure TX sensing subsection is generated even when there is no sectors in the pure networking case
        return output_str, rf_count, rf_vec, line_count
    else :
        return output_str, (line_count/rf_count), rf_vec, line_count

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def get_tx_sens_sector_address(dark_blue_output):
    gp_table_dict = dark_blue_output['GP_Labels']
    address = HW_DEFINITIONS[gp_table_dict[GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['TX_SECTOR_ADDRESS'] + get_rf_sens_first_ssw_sector_indx(dark_blue_output) *32
    return address

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_ini_output(dark_blue_output, ini_filename):
    
    section_list = [
        SectionIniCommonRf(dark_blue_output),
        SectionIniGeneralParameters(dark_blue_output)
    ]

    sectors_measured_to_full_channel_dict = get_sectors_measured_to_full_channel_list(dark_blue_output, 'SectorChannelGroups')
    if dark_blue_output['GP_Sens_Labels'] :
        sens_sectors_measured_to_full_channel_dict = get_sectors_measured_to_full_channel_list(dark_blue_output, 'SectorChannelGroups_sens')
    else:
        sens_sectors_measured_to_full_channel_dict = get_sectors_measured_to_full_channel_list(dark_blue_output, 'SectorChannelGroups')

    channel_groups_to_channels_dict = get_rf_configure_channel_group_list(dark_blue_output)
    
    gp_table_dict = dark_blue_output['GP_Labels']   
    
    if CUSTOMIZATIONS['TSSI_SECTOR'] != None:
        tssi_channels_group_dict = gp_table_dict['TSSICalibStructure']['ChannelGroups'] 
        for measured_channel in sorted(tssi_channels_group_dict.keys()):
            curr_tssi_sectors_section = SectionTssiSectors(tssi_channels_group_dict[measured_channel], dark_blue_output)
            section_list.append(curr_tssi_sectors_section)
    
    for measured_channel in sorted(sectors_measured_to_full_channel_dict.keys()):
        curr_rx_sector_section = SectionIniSectors(GENERIC_CONSTANTS['TAG_NAMES']['RX'], measured_channel, sectors_measured_to_full_channel_dict[measured_channel], dark_blue_output, HW_DEFINITIONS[gp_table_dict[GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['RX_SECTOR_ADDRESS'],'Sectors')
        section_list.append(curr_rx_sector_section)

    #TODO: change in the future to RX/TX per channel, printed like this now to enable simple diff
    for measured_channel in sorted(sectors_measured_to_full_channel_dict.keys()):
        curr_tx_sector_section = SectionIniSectors(GENERIC_CONSTANTS['TAG_NAMES']['TX'], measured_channel, sectors_measured_to_full_channel_dict[measured_channel], dark_blue_output, HW_DEFINITIONS[gp_table_dict[GENERIC_CONSTANTS['TAG_NAMES']['SILICON_NAME']]]['TX_SECTOR_ADDRESS'],'Sectors')
        section_list.append(curr_tx_sector_section)

    if CUSTOMIZATIONS['SUPPORT_SENSING'] == True:
        for measured_channel in sorted(sens_sectors_measured_to_full_channel_dict.keys()):
            tx_sens_sectors_address = get_tx_sens_sector_address(dark_blue_output)
            curr_sens_tx_sector_section = SectionIniSectors(GENERIC_CONSTANTS['TAG_NAMES']['TX'], measured_channel, sens_sectors_measured_to_full_channel_dict[measured_channel], dark_blue_output, tx_sens_sectors_address,'Sectors_sens')
            section_list.append(curr_sens_tx_sector_section)

    for channel_group in sorted(channel_groups_to_channels_dict.keys()):
        curr_rf_cfg_section = SectionRfChannelConfig(channel_groups_to_channels_dict[channel_group], dark_blue_output)
        section_list.append(curr_rf_cfg_section)

    output_str_header = write_ini_header(section_list)

    try:
        with open(ini_filename, 'w') as ini_out_file:
            ini_out_file.write(output_str_header)

            for curr_section in section_list:
                ini_out_file.write(str(curr_section))

            if CUSTOMIZATIONS['SUPPORT_SENSING'] == True and dark_blue_output['Sens_General_Parameters'] :
                ini_out_file.write(write_sens_ini_header())
                ini_out_file.write(generate_general_params(dark_blue_output))
                ini_out_file.write(generate_enable_vectors(dark_blue_output))
                ini_out_file.write(generate_pulse_params(dark_blue_output))
                ini_out_file.write(write_sens_ini_tail())
    except IOError as e:
        e.message=e.filename + ": " + e.strerror
        raise e

    
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def append_sens_to_ini(sens_params_ini, ini_filename):

    with open(sens_params_ini, 'r') as file:
        data = file.read()
    file.close()
    with open(ini_filename, 'a') as ini_out_file:
        ini_out_file.write(data)
    

def write_sens_ini_header():
    output_str = ''
    output_str += ("[SENSE_PARAMS]\n")
    output_str += (";; ---------------Sensing Parameters - Start ----------------------------\n")
    output_str += (";; sense_params start of section = version\n")
    output_str += write_ini_line (GEN_INI_CONSTANTS['BOARD_FILE_FORMAT_VERSION_SENS_VP'], 0, '') 
    output_str += (";; <number of modes> = <enabled modes>\n")
    output_str += (";; Eanebled modes bit vector \n")
    output_str += (";; 1: Search\n")
    output_str += (";; 2: Face Recognition\n")
    output_str += (";; 3: Gesture\n")
    output_str += (";; 4:\n")
    output_str += (";; 5:\n")
    output_str += (";; 6:\n")
    output_str += (";; 7:\n")
    output_str += (";; 8:\n")
    output_str += (";; 9:\n")
    output_str += (";; 10:\n")
    output_str += (";; 11:\n")
    output_str += (";; 12:\n")
    output_str += (";; 13:\n")
    output_str += (";; 14:\n")
    output_str += (";; 15: Test\n")
    output_str += write_ini_line (GEN_INI_CONSTANTS['NUMBER_OF_MODES_SENS'], GEN_INI_CONSTANTS['ENABLED_SENS_MODE'], '') 
    output_str += (";; enable vector of sensing burst\n")
    output_str += write_ini_line (GEN_INI_CONSTANTS['SUBTASK_MASK_SENS'], 0, '') 

    return output_str


def generate_general_params(dark_blue_output):
    CONSTANTS=init_CONSTANTS()
    output_str = ''
    vector_start_index = 0
    vector_end_index = 0

    for sens_mode in range(0, len(CONSTANTS['SENS_MODE'])):
        output_str += ";; general parameters for mode: " + CONSTANTS['SENS_MODE'][sens_mode] +"\n"
        output_str += write_ini_line(dark_blue_output['Sens_General_Parameters'][sens_mode]['watermark'],dark_blue_output['Sens_General_Parameters'][sens_mode]['t_burst'],'')
        output_str += write_ini_line(dark_blue_output['Sens_General_Parameters'][sens_mode]['n_bursts'],dark_blue_output['Sens_General_Parameters'][sens_mode]['t_pulse'],'')


        n_pulses = int(dark_blue_output['Sens_General_Parameters'][sens_mode]['n_pulses'])
        n_samples = int(dark_blue_output['Sens_General_Parameters'][sens_mode]['n_samples'])
        val1 = BitFieldDW(0,16,n_pulses)
        val1.add_field(16,16,n_samples)

        first_sample_offset = int(dark_blue_output['Sens_General_Parameters'][sens_mode]['first_sample_offset'])
        samples_to_avg = int(dark_blue_output['Sens_General_Parameters'][sens_mode]['samples_to_avg'])
        pulses_to_avg = int(dark_blue_output['Sens_General_Parameters'][sens_mode]['pulses_to_avg'])

        val2 = BitFieldDW(0,8,first_sample_offset)
        val2.add_field(8,8,samples_to_avg)
        val2.add_field(16,8,pulses_to_avg)
        val2.add_field(24,8,vector_start_index)
        output_str += write_ini_line(int(val1), int(val2),'')

        vector_end_index = vector_start_index + len(dark_blue_output['Sens_Enable_Vectors'][sens_mode]) - 1
        val1 = BitFieldDW(0,8,vector_end_index)
        val1.add_field(8,24,0)
        output_str += write_ini_line(int(val1), 0,'')

        vector_start_index += len(dark_blue_output['Sens_Enable_Vectors'][sens_mode])

    return output_str

def generate_enable_vectors(dark_blue_output):
    CONSTANTS=init_CONSTANTS()
    output_str = ''
    N_vector = 0
    pulse_table_offset = 0

    for sens_mode in range(0, len(CONSTANTS['SENS_MODE'])):
        output_str += ";; Enable Vecotrs for mode: " + CONSTANTS['SENS_MODE'][sens_mode] +"\n"
        N_config = len(dark_blue_output['Sens_Enable_Vectors'][sens_mode])
        for cnt_config in range(0, N_config):
            output_str += write_ini_line(pulse_table_offset, list_to_bit_vector(dark_blue_output['Sens_Enable_Vectors'][sens_mode][cnt_config]),'')
            pulse_table_offset += 32
            N_vector += 1

    if N_vector > 32 :
        raise Exception('Error: Number of vectors is greater than 32.')
    else:
        while N_vector < 32:
            output_str += write_ini_line(0,0,'')
            N_vector += 1
    return output_str

def get_valid_antenna_vec_sens(dark_blue_output):

    sensing_RFset_index = 0    # Currently we only have 1 pair of sensing TX RF and RX RF. The corresponding RF set has index 0

    tx_rf_list = dark_blue_output['GP_Sens_Labels']['RFsets_dict'][sensing_RFset_index]['TX_RFCs']
    if len(tx_rf_list) > 1:
        raise Exception('More than one TX RF for sensing. Currently only one TX RF is supported.')
    if len(tx_rf_list) < 1:
        raise Exception('No information on TX RF for sensing.')
    tx_rf = tx_rf_list[0]

    rx_rf_list = dark_blue_output['GP_Sens_Labels']['RFsets_dict'][sensing_RFset_index]['RX_RFCs']
    if len(rx_rf_list) > 1:
        raise Exception('More than one RX RF for sensing. Currently only one RX RF is supported.')
    if len(rx_rf_list) < 1:
        raise Exception('No information on RX RF for sensing.')
    rx_rf = rx_rf_list[0]

    valid_anntena_vec_tx = valid_to_invalid_anntennas(dark_blue_output['GP_Sens_Labels']['BRPStructure'][tx_rf])
    valid_anntena_vec_rx = valid_to_invalid_anntennas(dark_blue_output['GP_Sens_Labels']['BRPStructure'][rx_rf])

    return valid_anntena_vec_tx, valid_anntena_vec_rx


def generate_pulse_params(dark_blue_output):
    CONSTANTS=init_CONSTANTS()
    output_str = ''
    N_pulse = 0
    valid_antenna_vec_tx, valid_antenna_vec_rx = get_valid_antenna_vec_sens(dark_blue_output)

    for sens_mode in range(0, len(CONSTANTS['SENS_MODE'])):
        N_pulse_per_mode = 0
        output_str += ";; Pulse Parameters for mode: " + CONSTANTS['SENS_MODE'][sens_mode] +"\n"
        N_txrx_comb = len(dark_blue_output['Sens_Pulse_Params'][sens_mode])
        for cnt_txrx_comb in range(0, N_txrx_comb):
            N_tx = len(dark_blue_output['Sens_Pulse_Params'][sens_mode][cnt_txrx_comb]['tx_sector'])
            N_rx = len(dark_blue_output['Sens_Pulse_Params'][sens_mode][cnt_txrx_comb]['rx_sector'])
            for cnt_rx in range(0, N_rx):
                for cnt_tx in range(0, N_tx):
                    tx_sector = int(dark_blue_output['Sens_Pulse_Params'][sens_mode][cnt_txrx_comb]['tx_sector'][cnt_tx])
                    rx_sector = int(dark_blue_output['Sens_Pulse_Params'][sens_mode][cnt_txrx_comb]['rx_sector'][cnt_rx])

                    if tx_sector not in valid_antenna_vec_tx:
                        raise Exception('Error in sensing mode %s pulse parameter configuration: sensing TX sector %d is invalid.'% (CONSTANTS['SENS_MODE'][sens_mode], tx_sector))
                    if rx_sector not in valid_antenna_vec_rx:
                        raise Exception('Error in sensing mode %s pulse parameter configuration: sensing RX sector %d is invalid.'% (CONSTANTS['SENS_MODE'][sens_mode], rx_sector))

                    val = BitFieldDW(0,8,rx_sector)
                    val.add_field(8,16,0)
                    val.add_field(24,8,tx_sector)
                    output_str += write_ini_line(int(val), 0,'')
                    N_pulse_per_mode += 1

        while N_pulse_per_mode < len(dark_blue_output['Sens_Enable_Vectors'][sens_mode]) * 32 :
            output_str += write_ini_line(0, 0,'')
            N_pulse_per_mode += 1

        N_pulse += N_pulse_per_mode

    if N_pulse > 1024:
        raise Exception('Error: Number of pulses is greater than 1024.')
    return output_str

def write_sens_ini_tail():
    output_str = (";; ---------------Sensing Parameters - End ----------------------------\n")
    return output_str