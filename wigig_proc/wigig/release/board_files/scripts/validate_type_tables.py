#-------------------------------------------------------------------------------
# Name:        type tables module
# Purpose:
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

import xml.etree.ElementTree as ET
from useful_functions import *
from customizations import *

def validate_classification_xml(classification_root):
    #
    #
    #

    CONSTANTS=init_CONSTANTS()

    error_msg=''

    #get Group names from xml
    group_names=specific_labels_in_one_tag(classification_root,
        CONSTANTS['TAG_NAMES']['GROUP'],
        CONSTANTS['ATTRIB_NAMES']['NAME'])

    #internalize dictionary
    rf_config_channel_groups=dict.fromkeys(group_names)

    for group in classification_root.findall(CONSTANTS['TAG_NAMES']['GROUP']):
        #get group name
        curr_group=group.get(CONSTANTS['ATTRIB_NAMES']['NAME'])

        #add element to rf_config_channel_groups dictionary
        try:
            rf_config_channel_groups[curr_group]=validate_type_tables_xml(group)
        except AssertionError as e:
            error_msg=(error_msg +"In Group {0}:\n".format(curr_group) + e.message)

    assert(error_msg==''),error_msg

    return rf_config_channel_groups

def validate_type_tables_xml(type_tables_root):
    #
    # Validates one <Group> of type tables
    #

    error_msg=''

    #validate type tables
    try:
        validate_type_tables(type_tables_root);
    except AssertionError as e:
        error_msg+=e.message

    #generate rf config channel groups
    try:
        rf_config_channel_groups=generate_rf_config_dict(type_tables_root)
    except AssertionError as e:
        error_msg+=e.message

    assert(error_msg==''),error_msg

    return rf_config_channel_groups

def validate_type_tables(Group_element):
    # This function completes the validation
    # of the type tables xml.  It asssumes
    # the xml has been validated by the
    # corresponding *.xsd file.

    CONSTANTS=init_CONSTANTS()

    #easier notation
    ALL_TABLES_LIST=CONSTANTS['ALL_TABLES_LIST']
    Ntables=len(ALL_TABLES_LIST);

    #initialize variables
    legal_group=False;
    error_msg=''

    #initialize
    legal_table=[False] * Ntables

    #check each table
    for cnt_table in range(0,Ntables):
        legal_table[cnt_table],internal_err=check_legal_table(
            Group_element,ALL_TABLES_LIST[cnt_table])
        error_msg+=internal_err
    legal_group=all(legal_table)

    if (not legal_group):
        error_msg+= \
            "Each table must appear only in rf common or appear in all channels\n"

    assert(error_msg==''),error_msg

def check_legal_table(root,table_name):
    # This function determines if a given table
    # has valid status.  'Valid status' means that
    # exactly one of the following is true:
    # a) the table appears in the common section only
    # b) the table appears in each of the defined channels,
    # but not in the common section

    CONSTANTS=init_CONSTANTS()

    #find all direct subelements of root
    all_channels=root.findall(CONSTANTS['TAG_NAMES']['CHANNEL']);
    all_channels.insert(0,root.find(CONSTANTS['TAG_NAMES']['COMMON_TYPE_TABLES']));

    #define constant
    Nsections=len(all_channels)

    #initialize variable
    table_in_channel=[True] *(Nsections)
    error_msg=''

    #find all sections where 'table_name' appears
    for cnt_channel in range(0,Nsections):
        if (all_channels[cnt_channel][0].find(table_name)) is None: #idx 0 is element 'all_tables'
            table_in_channel[cnt_channel]=False;

    #is table in common section?
    table_is_in_common=table_in_channel[0];

    #how many channels include table?
    table_is_in_all_channels=all(table_in_channel[1:]);
    table_is_in_some_channels=(any(table_in_channel[1:])) and (not table_is_in_all_channels)
    not_table_in_channel=[not x for x in table_in_channel]
    table_is_in_no_channel=all(not_table_in_channel[1:]);

    #all logical clauses (there is one special table)
    if table_name=='additional_fixes':
        if table_is_in_no_channel:
            return True,error_msg
        else:
            error_msg+="table " + table_name + \
                " cannot occur in channel-specific RF config\n"
            return False,error_msg
    else:
        if table_is_in_common and table_is_in_no_channel:
            return True,error_msg
        elif (not table_is_in_common) and (table_is_in_all_channels):
            return True,error_msg
        elif table_is_in_common and (table_is_in_some_channels or table_is_in_all_channels):
            error_msg+=("table " + table_name + \
                " is found in both common rf and some channels.\n")
            return False,error_msg
        elif (not table_is_in_common) and table_is_in_some_channels:
            error_msg+=("table " + table_name + \
                " is only found in some channels.\n")
            return False,error_msg
        elif (not table_is_in_common) and table_is_in_no_channel:
            error_msg+=("Table " + table_name + \
                " cannot be found anywhere in the <Group> element.\n")
            return False,error_msg
        else:
            error_msg+=("logical clauses in check_legal_table.  Code should not be here\n")
            return False,error_msg
    end

def generate_rf_config_dict(Group_element):
    # This function maps each channel to its channel group.
    # It checks the xml that each channel in CONSTANTS['SUPPORTED_CHANNELS']
    # appears once and only once in the *.xml

    CONSTANTS=init_CONSTANTS()

    #initialize outputs
    rf_config_dict={}
    error_msg=''

    #initialize dict entry
    rf_config_dict=dict.fromkeys(CONSTANTS['SUPPORTED_CHANNELS'])
    
    #get channel groups as defined in xml
    channel_list=specific_labels_in_one_tag(Group_element,CONSTANTS['TAG_NAMES']['CHANNEL']
        ,CONSTANTS['ATTRIB_NAMES']['NUM'])
    for ch_group in range(0,len(channel_list)):
        if isinstance(channel_list[ch_group], (int, long)):
            channel_list[ch_group]=[channel_list[ch_group]]
        for channel in channel_list[ch_group]:
            if  rf_config_dict[channel] is None:
                rf_config_dict[channel]=ch_group
            else: #if this channel already appeared
                error_msg+="Channel {0} appears more than once in RF config section\n".format(channel)
    
    for channel in rf_config_dict.keys():
        if rf_config_dict[channel] is None:
            if channel in CUSTOMIZATIONS['SUPPORTED_CHANNELS_IN_BOARD_FILE']:
                error_msg+=("Channel {0} is not found in RF config section\n".format(channel))
            else:
                rf_config_dict.pop(channel)
                

    assert(error_msg==''),error_msg
    
    return rf_config_dict