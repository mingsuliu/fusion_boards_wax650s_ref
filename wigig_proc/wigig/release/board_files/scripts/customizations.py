#-------------------------------------------------------------------------------
# Name:        customizations.py
# Purpose:     customizations layer which allow to customize the behavior of the tool per branch
#
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

#define different behavior of the script per FW branch
CUSTOMIZATIONS={
                'RF_SETS_MAPPING' : 'V1',
                'ALLOW_EMPTY_RF_SET' : False, #True for automotive, False for networking and sensing
                'SUPPORT_SENSING' : True,
                'ADD_TPC_LIMIT' : True,
                'ADD_LO_POWER_TARGET_LIMIT' : True,
                'SWITCHS_RX_SECTOR' : None, # specify None to place it in the after the last Rx sector
                'TSSI_SECTOR' : 97, # specify None avoid supporting TSSI sector
                'SUPPORTED_CHANNELS_IN_BOARD_FILE' : [1,2,3,4,5,9,10,11,12], #Specify which channels are supported out of the available channels
                'NUM_OF_SECTIONS_PER_CHANNEL' : 5, #TSSI, TX, RX, TX sensing, RF config
                'PER_CHANNEL_DESC_LIST' : [' ;; TSSI sectors', ' ;; RX sectors', ' ;; TX sectors', ' ;; TX Sensing sectors', ' ;; RF config'],
                'TX_SECTORS_PADDING' : None, # specify None to avoid padding or specify the maximum sector id to be padded
                'MAX_SECTORS_DIVERSITY' :63,
                'MAX_SECTORS_SINGLE_TILE' : 63,
                'CONCATE_DIVERSITY_SECTORS' :False,
                'FW_VERSION' : "7109999"
               }