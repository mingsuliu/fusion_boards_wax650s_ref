#-------------------------------------------------------------------------------
# Name:        Sectors
# Purpose:     Generates the data structure containing the sectors for each
#              channel, and also allocates sectors to channels not measured
#
# Copyright (c) 2019 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#-------------------------------------------------------------------------------

from useful_functions import *
from sector_class import Sector

def _iter_python26(node, string):
  return node.findall(string)

def generate_sector_struct(RF_sets_element,RF_sets_structure):
    #
    # This function reads all of the sectors from the *.xml files, processes
    # them, and convert the data to a Python nested dictionary
    #

    #get constants
    CONSTANTS=init_CONSTANTS()

    #initialize output
    error_flag=False
    error_message=''

    #get channel list
    all_channels=RF_sets_structure[0].keys()

    #get aggregation of all active RFCs
    active_RX_RFCs=[]
    active_TX_RFCs=[]
    for RFset in RF_sets_structure.keys():
        active_RX_RFCs.extend(RF_sets_structure[RFset][all_channels[0]]['rx'].keys())
        active_TX_RFCs.extend(RF_sets_structure[RFset][all_channels[0]]['tx'].keys())

    #initialize sector dictionary
    sector_dict=dict.fromkeys(all_channels)
    for channel in sector_dict.keys():
        sector_dict[channel]=dict.fromkeys(['rx','tx'])
        sector_dict[channel]['rx']=dict.fromkeys(active_RX_RFCs)
        sector_dict[channel]['tx']=dict.fromkeys(active_TX_RFCs)

        for RX_RFC in active_RX_RFCs:
            sector_dict[channel]['rx'][RX_RFC]={}
        for TX_RFC in active_TX_RFCs:
            sector_dict[channel]['tx'][TX_RFC]={}

    #get all RF set tree elements
    for RFset_element in _iter_python26(RF_sets_element, CONSTANTS['TAG_NAMES']['RF_SET']):
        RF_set=RFset_element.get(CONSTANTS['ATTRIB_NAMES']['NUM'])
        for channel_element in _iter_python26(RFset_element, CONSTANTS['TAG_NAMES']['CHANNEL']):
            channel=int(channel_element.get(CONSTANTS['ATTRIB_NAMES']['NUM']))
            for txrx_element in list(channel_element):
                #easier notation
                mode=txrx_element.tag.lower()

                for RFC_element in _iter_python26(txrx_element, CONSTANTS['TAG_NAMES']['RFC']):
                    RFC=int(RFC_element.get(CONSTANTS['ATTRIB_NAMES']['NUM']))
                    for sector_element in _iter_python26(RFC_element, CONSTANTS['TAG_NAMES']['SECTOR']):
                        sector_num=int(sector_element.get(
                            CONSTANTS['ATTRIB_NAMES']['NUM']))
                        try:
                            sector_dict[channel][mode][RFC][sector_num]=\
                                Sector(xml_input=sector_element,direction=mode)
                        except AssertionError as e:
                            e.message+=" RFC {0}, channel {1}, {2}, sector {3}\n"\
                                .format(RFC,channel,mode,sector_num)
                            error_message+=e.message
                            error_flag=True
                        except Exception as e:
                            error_message+=e.message
                            error_flag=True
                            raise
    if error_flag:
        raise AssertionError(error_message)

    sectors_channel_dict=generate_sector_channel_dict(all_channels)

    return sector_dict,sectors_channel_dict

def generate_sector_channel_dict(measured_channels):
    #
    # If not all channels have been measured, this function determines
    # the sectors used for each of the channels
    #
    
    #constants
    CONSTANTS=init_CONSTANTS()
    all_channels=range(min(CONSTANTS['SUPPORTED_CHANNELS']), 1+max(CONSTANTS['SUPPORTED_CHANNELS']))
    Nchannels=len(all_channels)
    Nmeasured=len(measured_channels)
    NAN = 999; #high number to be used as marker for non relevant cells

    #init distance matrix
    channel_distance_table = [[0.0,1.0,2.0,3.0,4.0,5.0,NAN,NAN,0.5,1.5,2.5,3.5], # ch 1
                              [1.0,0.0,1.0,2.0,3.0,4.0,NAN,NAN,0.5,0.5,1.5,2.5], # ch 2
                              [2.0,1.0,0.0,1.0,2.0,3.0,NAN,NAN,1.5,0.5,0.5,1.5], # ch 3
                              [3.0,2.0,1.0,0.0,1.0,2.0,NAN,NAN,2.5,1.5,0.5,0.5], # ch 4
                              [4.0,3.0,2.0,1.0,0.0,1.0,NAN,NAN,3.5,2.5,1.5,0.5], # ch 5
                              [5.0,4.0,3.0,2.0,1.0,0.0,NAN,NAN,4.5,3.5,2.5,1.5], # ch 6
                              [NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN], # ch 7
                              [NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN,NAN], # ch 8
                              [0.5,0.4,1.5,2.5,3.5,4.5,NAN,NAN,0.0,1.0,2.0,3.0], # ch 9 - give prio to channel 2
                              [1.5,0.4,0.5,1.5,2.5,3.5,NAN,NAN,1.0,0.0,1.0,2.0], # ch 10 - give prio to channel 3
                              [2.5,1.5,0.5,0.4,1.5,2.5,NAN,NAN,2.0,1.0,0.0,1.0], # ch 11 - give prio to channel 4
                              [3.5,2.5,1.5,0.5,0.5,1.5,NAN,NAN,3.0,2.0,1.0,0.0]] # ch 12

    #clear non relevant cells since they were not measured
    for col_channel in all_channels:
        if col_channel not in measured_channels:
            #need to clear it
            for row_channel in all_channels:
                channel_distance_table[row_channel-1][col_channel-1]=NAN
            
    #find nearest measured channel to each supported channel
    nearest_measured_ch=dict.fromkeys(CONSTANTS['SUPPORTED_CHANNELS'])
    for channel in CONSTANTS['SUPPORTED_CHANNELS']:
        nearest_measured_ch[channel]= 1 + channel_distance_table[channel-1].index(min(channel_distance_table[channel-1]))

    return nearest_measured_ch



